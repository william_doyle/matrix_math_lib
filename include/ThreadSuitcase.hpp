#pragma once

/*
 * ThreadSuitcase.hpp
 * William Doyle
 * To be cast to and from a void pointer to allow threads access
 * to resources
 *
 * */
class ThreadSuitcase {
	public:
		int thread_id;
		ThreadSuitcase (int);
		virtual void* AsVoidPointer();
};
