#pragma once
#include <ostream>
/*
	Atom.hpp
	Wdd
	Define the most basic datatype. This will be shared between all members of a matrix and between possably all matrixes in a system.

*/
class atom_type {
		virtual atom_type& operator+=(const atom_type&) = delete;
		virtual atom_type& operator-=(const atom_type&) = delete;
		virtual atom_type & operator * (const atom_type&) = delete;	
		virtual atom_type & operator / (const atom_type&) = delete;
		virtual atom_type & operator / (double) = delete;
		virtual bool operator == (const atom_type &) = delete;
		virtual bool operator > (const atom_type &) = delete;
		virtual bool operator < (const atom_type &) = delete;
		virtual atom_type & operator + (const atom_type &) = delete;
		virtual atom_type & operator - (const atom_type &) = delete;
};

// The most basic type in the system
//typedef double Atom;
// change name to DoubleAtom 
class Atom {
//class Atom : public Operand, public atom_type{
	public:
		// Constructors
		Atom();
		Atom(unsigned);
		Atom(int);
		Atom(double);
		std::string GetType() ;

		// Operator static functions
		static Atom ADD_FUNC(Atom, Atom);
		static Atom SUB_FUNC(Atom, Atom);
		static Atom MULT_FUNC(Atom, Atom);

		double value;

		virtual Atom& operator+=(const Atom& rhs);
		virtual Atom& operator-=(const Atom& rhs);
		virtual Atom operator * (Atom rhs);			// note to self (nts) I need to understand this better. Why no '&' and no 'const'
		virtual Atom operator / (Atom rhs);
		virtual Atom operator / (double rhs);
		virtual bool operator == (Atom a);
		virtual bool operator > (Atom a);
		virtual bool operator < (Atom a);

		virtual Atom operator + (Atom a);
		virtual Atom operator - (Atom a);

		friend std::ostream &operator << (std::ostream &out, const Atom &A) {
			out << A.value;
			return out;
		};

};

class VectorAtom2D : public Atom {
	public:
		double x_comp, y_comp;

		virtual Atom& operator+=(const Atom& rhs) override ;
		virtual Atom& operator-=(const Atom& rhs) override ;
		virtual Atom operator * (Atom rhs) override ;			// note to self (nts) I need to understand this better. Why no '&' and no 'const'
		virtual Atom operator / (Atom rhs) override ;
		virtual Atom operator / (double rhs) override ;
		virtual bool operator == (Atom a) override ;
		virtual bool operator > (Atom a) override ;
		virtual bool operator < (Atom a) override ;

		virtual Atom operator + (Atom a) override ;
		virtual Atom operator - (Atom a) override ;

		///
		virtual VectorAtom2D& operator+=(const VectorAtom2D& rhs) ;
		virtual VectorAtom2D& operator-=(const VectorAtom2D& rhs) ;
		virtual VectorAtom2D operator * (VectorAtom2D rhs) ;			// note to self (nts) I need to understand this better. Why no '&' and no 'const'
		virtual VectorAtom2D operator / (VectorAtom2D rhs) ;
		virtual bool operator == (VectorAtom2D a) ;
		virtual bool operator > (VectorAtom2D a) ;
		virtual bool operator < (VectorAtom2D a) ;

		virtual VectorAtom2D operator + (VectorAtom2D a) ;
		virtual VectorAtom2D operator - (VectorAtom2D a) ;

};

// A pointer to an Atom
typedef Atom* Atom_ptr;
