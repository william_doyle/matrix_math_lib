#pragma once
#include "Matrix.hpp"
#include <memory>
/*
 *		January 6th 2021
 *		ColorImage.hpp
 *		Describe the ColorImage class
 *		William Doyle
 *
 * */

class ColorImage {
	public:
		std::shared_ptr<Matrix> reds;
		std::shared_ptr<Matrix> greens;
		std::shared_ptr<Matrix> blues;

		std::string format;
		unsigned number_cols; 
		unsigned number_rows; 
		unsigned max_color_value; // usually 255

		ColorImage(std::string);	// construct from filename
		ColorImage(Matrix, Matrix, Matrix, std::string = "P3");	// construct from rgb components
		~ColorImage();

		void Save(std::string);
	
		ColorImage Convolution(Matrix, int );

};
