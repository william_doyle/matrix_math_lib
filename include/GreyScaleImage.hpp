#pragma once
#include "Matrix.hpp"
#include <string>
#include <memory>
#include "ColorImage.hpp"

/**
 *	William Doyle
 *	Extends Matrix
 *	GreyScaleImage
 *
 *
 * */
class GreyScaleImage : public Matrix {
	protected:
		GreyScaleImage ClearClone();
		void ___save(std::ofstream*, std::string);
		
	public:
		std::string format;
		unsigned number_cols; 
		unsigned number_rows; 
		unsigned max_color_value; // usually 255

		Matrix _matrix;
	//	std::vector<Atom> * values;
		std::shared_ptr<std::vector<Atom>> values;
		GreyScaleImage(std::string);
		GreyScaleImage(ColorImage);
		GreyScaleImage(unsigned, unsigned);
		~GreyScaleImage();
		

		GreyScaleImage(Matrix values,  std::string fmat = "P3");
		void Save(std::string);
		void Save(std::string, std::string);
		void Clear();

		static void* range_convolve(void*);
		GreyScaleImage Sharpen();
		
		GreyScaleImage Convolution(Matrix, int numThreads = 1);

		GreyScaleImage operator + (GreyScaleImage gsi);
		GreyScaleImage operator - (GreyScaleImage gsi);

};

/*
 *	November 28th 2020
 *	William Doyle
 *	WriteAt_PlusThree()
 *	arg count -> 4
 *	arg types -> 2 int, Atom, GreyScaleImage*
 *	arg desc  -> position part, position part, value to write, ptr to image to write to
 *	Primarily just to condense code I know is safe while I look for bugs
 *	Should consider removing this method after bugs are found because this method is potentually
 *	unnesairy abstraction and confusion 
 * */
static inline bool WriteAt_PlusThree(int i, int k, Atom __value, GreyScaleImage* result_img ){
	if ( (result_img->WriteAt( i, k,   (Atom)__value ) == false) || (result_img->WriteAt( i, k+1, (Atom) __value ) == false) || (result_img->WriteAt( i, k+2, (Atom) __value ) == false) ){

		std::cerr << __func__ << " failed!\n";

		return false;
	}
	return true;
}



