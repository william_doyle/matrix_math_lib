#pragma once
#include "Atom.hpp"
#include <memory>
/*
 *	William Doyle
 *	January 8th 2021
 *	Pixel.hpp
 *	Define a pixel
 *
 * */

class Pixel {
	private:
		std::shared_ptr<Atom> red;	
		std::shared_ptr<Atom> green;	
		std::shared_ptr<Atom> blue;	
	public:
		Pixel(Atom, Atom, Atom);
		~Pixel();

		virtual Atom GetRed();						// get by value
		virtual Atom GetGreen();
		virtual Atom GetBlue();
		
		virtual std::shared_ptr<Atom> AccessRed();	// get by reference
		virtual std::shared_ptr<Atom> AccessGreen();
		virtual std::shared_ptr<Atom> AccessBlue();
};


class MonoPixel : public Pixel {
	private:
		std::shared_ptr<Atom> value;
	public:
		MonoPixel(Atom);
		~MonoPixel();

		virtual Atom GetRed() override;	
		virtual Atom GetGreen() override;	
		virtual Atom GetBlue() override;	
	
		virtual std::shared_ptr<Atom> AccessRed() override;	// get by reference
		virtual std::shared_ptr<Atom> AccessGreen() override;
		virtual std::shared_ptr<Atom> AccessBlue() override;


};



