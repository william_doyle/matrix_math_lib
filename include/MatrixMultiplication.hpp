#pragma once
#include "MultiThreadedMatrixOperation.hpp"


class MatrixMultiplication : public MultiThreadedMatrixOperation {
	protected:
		Matrix* lhs;
		Matrix* rhs;
	//	Matrix* result;
	public:
		MatrixMultiplication(Matrix, Matrix);
		MatrixMultiplication();
		~MatrixMultiplication();
		virtual Matrix ComputeOperation(Atom(*f)(Atom, Atom)) override;

		/*  	S T A T I C   F U N C T I O N S   */
		static unsigned Max(unsigned, unsigned, unsigned);
		static void recursive_multiply(Matrix*, Matrix*, Matrix*, unsigned, unsigned, unsigned, unsigned, unsigned, unsigned
				);
		static void pointer_multiply(Matrix*, Matrix*, Matrix*, unsigned, unsigned, unsigned, unsigned, unsigned, unsigned);


};
