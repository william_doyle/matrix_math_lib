#pragma once
#include "Matrix.hpp"
#include "MatrixException.hpp"
#include "MatrixMultiplication.hpp"
/*
 *	SmartOperators.hpp
 *	Simple classes allow for easy bridge between Matrix operator functions and 
 *	the optimal method for applying such an operator
 *	December 5th 2020
 *	William Doyle
 *  what if my operators were smart enough to consider avalable system resources when determining how best to behave?
 *
 * */

/*	December 5th 2020
 *	Simple base class for other smart operators
 * */
class SmartOperator {
	public:
		Matrix lhs;
		Matrix rhs;
		virtual Matrix Do() = 0;
		inline SmartOperator (Matrix _lhs, Matrix _rhs){
			this->lhs = _lhs;
			this->rhs = _rhs;
		};

};

#define SLOW_LIM 25


/*	Do addition intellegently
 *	Wdd
 *	December 5th 2020
 *
 * */
class SmartAdd: public SmartOperator {
	public:
		inline SmartAdd(Matrix _lhs, Matrix _rhs): SmartOperator(_lhs, _rhs) {
				
		};

		inline Matrix Do() {
				return slow_add(&(this->rhs), this->lhs);
			if ( (this->rhs.Width() != this->lhs.Width()) || (this->rhs.Length() != this->lhs.Length()) ) {
				throw UnevenMatrixException(&(this->rhs), &(this->lhs));
			}		
			if (this->rhs.Length()*this->rhs.Width() < SLOW_LIM)
				return slow_add(&(this->rhs), this->lhs);
			
			MultiThreadedMatrixOperation oper;
			oper.operands.push(this->lhs);
			oper.operands.push(this->rhs);
			return oper.ComputeOperation(Atom::ADD_FUNC);
		}
};

/*
 *	Do multiplication intellegently
 *	wdd
 *	December 5th 2020
 *
 *
 * */
class SmartMult: public SmartOperator {
	public:
		int l;
		int w;

		inline SmartMult(Matrix _lhs, Matrix _rhs): SmartOperator(_lhs, _rhs) {

		};

		inline Matrix Do() {
	//		assert(this->rhs.Length() == this->lhs.Width());
			return NaiveMultiply(&(this->rhs), &(this->lhs));
			/*
			if (this->rhs.Length()*this->rhs.Width() < SLOW_LIM){
				return slow_mult(&(this->rhs), this->lhs);
			}
			
			return MatrixMultiplication(this->lhs, this->rhs).ComputeOperation(Atom::MULT_FUNC);
			*/
		};
};

/*
 *	Do subtraction intellegently
 *	wdd
 *	December 5th 2020
 *
 *
 * */
class SmartSubtract: public SmartOperator {
	public:
		inline SmartSubtract(Matrix _lhs, Matrix _rhs): SmartOperator(_lhs, _rhs) {
				
		};

		inline Matrix Do() {
			if ( (this->rhs.Width() != this->lhs.Width()) || (this->rhs.Length() != this->lhs.Length()) ) {
				throw UnevenMatrixException(&(this->rhs), &(this->lhs));
			}		
		
			return slow_subtract(&(this->lhs), this->rhs);
			
			if (this->rhs.Length()*this->rhs.Width() < SLOW_LIM)
				return slow_subtract(&(this->lhs), this->rhs);
			
			MultiThreadedMatrixOperation oper;
			oper.operands.push(this->rhs);
			oper.operands.push(this->lhs);
			return oper.ComputeOperation(Atom::SUB_FUNC);
		}
};
