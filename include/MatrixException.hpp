#pragma once
#include <exception>
#include <iostream>
#include "Matrix.hpp"
#include <string>

class MatrixException : public std::exception{
	protected:
		std::string error_msg;
	public:
		const inline char * what() const throw () {
			return "Matrix Exception!";
		};
};


/**
 *	William Doyle
 *	year 2020
 *	Uneven Matrix Exception
 *
 *
 * */
class UnevenMatrixException: public MatrixException {
	public:
		Matrix* matrix1;
		Matrix* matrix2;

		inline UnevenMatrixException (Matrix* _m1, Matrix* _m2){

			this->matrix1 = _m1;
			this->matrix2 = _m2;

			std::stringstream ss;
			ss << "Matricies are uneven in ";
			if (this->matrix1->Width() != this->matrix2->Width()){
				ss << "width ";
				if (this->matrix1->Length() != this->matrix2->Length())
					ss << "and "; 
			}
			if (this->matrix1->Length() != this->matrix2->Length())
				ss << "Length.";

			this->error_msg =  ss.str();

		};

		const inline char * what() const throw () {
			return this->error_msg.c_str();
		}

};
