#pragma once
#include <stack>
#include "Matrix.hpp"
/*
 *	December 4th 2020
 *	MatrixOperation.hpp
 *	Define the base class for the operation hierarchy.
 *
 * */

class MatrixOperation {
	public:
		std::stack<Matrix> operands;
		virtual Matrix ComputeOperation(Atom(*f)(Atom,Atom)) = 0;


};
