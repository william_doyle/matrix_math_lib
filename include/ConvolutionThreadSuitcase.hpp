#pragma once
#include "Matrix.hpp"
#include "GreyScaleImage.hpp"
#include "ThreadSuitcase.hpp"

/*	Wdd
 *	November 30th 2020
 *	package relevent data for the threads that convolve the image
 *
 * */


class ConvolutionThreadSuitcase : public ThreadSuitcase {
	public:
		int thread_id;
		Matrix M;
		GreyScaleImage* _this;
		unsigned i_start;
		unsigned i_end;
		unsigned k_start;
		unsigned k_end;
		GreyScaleImage* result_img_ptr;

		ConvolutionThreadSuitcase(
				int, 
				Matrix, 
				GreyScaleImage* , 
				unsigned ,
			       	unsigned , 
				unsigned , 
				unsigned , 
				GreyScaleImage* 
				);
		void* AsVoidPointer() override;
};



class ConvolutionThreadSuitcase_m : public ThreadSuitcase {
	public:
		int thread_id;
		Matrix M;
		Matrix* _this;
		unsigned i_start;
		unsigned i_end;
		unsigned k_start;
		unsigned k_end;
		Matrix* result_ptr;

		ConvolutionThreadSuitcase_m(
				int, 
				Matrix, 
				Matrix* , 
				unsigned ,
			       	unsigned , 
				unsigned , 
				unsigned , 
				Matrix* 
				);
	//	void* AsVoidPointer() override;
};

