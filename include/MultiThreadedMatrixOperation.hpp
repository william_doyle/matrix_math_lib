#pragma once
#include "MatrixOperation.hpp"
#include <pthread.h>
/*
 * MultiThreadedMatrixOperation.cpp
 * December 4th 2020
 * Wdd
 * */


class MultiThreadedMatrixOperation: public MatrixOperation {
	public: 
		unsigned number_threads;
		static void* range_do(void* targ);
		virtual Matrix ComputeOperation(Atom(*f)(Atom, Atom));
};
