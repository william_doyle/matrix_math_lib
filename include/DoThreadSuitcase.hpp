#pragma once
#include "ThreadSuitcase.hpp"

/*
 *	William Doyle
 *	DoThreadSuitcase.hpp
 *
 * */

class DoThreadSuitcase: public ThreadSuitcase {
	public:
		Matrix* result;
		Matrix* matrix1;
		Matrix* matrix2;
		unsigned start_i;
		unsigned end_i;
		unsigned start_k;
		unsigned end_k;
		Atom (*FUNCTION)(Atom, Atom);

		/*
		 *	Suitcase constructor
		 *
		 * */
		inline DoThreadSuitcase( int tid, Matrix* _resultmtxptr, Matrix* m1, 
				Matrix* m2, unsigned _start_i, unsigned _end_i, unsigned _start_k, unsigned _end_k, Atom(*__func)(Atom, Atom)): ThreadSuitcase(tid){
			this->result = _resultmtxptr;
			this->matrix1 = m1;
			this->matrix2 = m2;
			this->start_i = _start_i;
			this->end_i = _end_i;
			this->start_k = _start_k;
			this->end_k = _end_k;
			this->FUNCTION = __func;
		};

		inline void* AsVoidPointer()  {
			return (void*)this;
		};
};
