#pragma once
#include "Matrix.hpp"
/**

	williamdoyle.ca
	William Doyle
	
	Info:	https://en.wikipedia.org/wiki/Kernel_(image_processing)
	


*/

class Kernel {

	public:
		static Matrix Blur();
		static Matrix EdgeDetect(short type_id = 0);
		static Matrix Sharpen();
	   	static Matrix Random(int = -16, int = 16);	


};
