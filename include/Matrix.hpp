#pragma once 

#include <vector>
#include <iostream>
#include <ostream>
#include <stack>
#include <iomanip>
#include "Atom.hpp"
#include <cmath>

/*
	WDD
	Matrix.hpp
		Define the class Matrix. A Matrix is a rectangular array of numbers. It has roes and coloms 

		https://en.wikipedia.org/wiki/Matrix_(mathematics) 


*/

class Matrix {
	public:
		std::vector<std::vector<Atom>> matrix_members;		// holds values of matrix and preserves order
	
		static void* _range_convolve(void*);
		Matrix _convolution(Matrix, int numThreads = 1);
		Matrix Expand(unsigned = 3);

		std::string generate_ppm_comment();	
		static Matrix FromCsvFile(std::string);
		void SaveAsCsvFile(std::string);
		void PopulateRandomValuesInRange(int, int);
		Matrix Glue(std::string, Matrix); // attach a matrix to this matrix and return the result
		Matrix* copy();
		unsigned Width() const;
		unsigned Length() const;
		std::string GetType(); 
		Atom GetAverage()const;
		Atom GetSum() const;
		Atom Max();
		Atom SmallestValue();
		Atom_ptr Access(unsigned, unsigned);

		Atom_ptr ReadAt(unsigned, unsigned);	// returns pointer to the atom found at specified point
		bool WriteAt(unsigned, unsigned, Atom); // writes atom to address in the matrix. returns true on success

		Matrix SubMatrixByRange(unsigned, unsigned, unsigned, unsigned);
		Matrix SubMatrixByQuarter(int);

		Matrix();
		Matrix(unsigned, unsigned, double*);
		virtual ~Matrix();
		Matrix (unsigned, unsigned); // sets up a matrix of defined dimentions
		virtual void Establish(unsigned, unsigned);
	
		/*	DiagonalSize()
		 *	December 13th 2020
		 *	returns the distance between two ajacent corners of this rectangular matrix
		 * */
		inline double DiagonalSize(){
			// pythagonian therum
			return (double)sqrt( pow(this->Width(), 2) + (double)pow(this->Length(), 2) );

		}

		/*	December 13th 2020
		 *	William Doyle
		 *	Size()
		 *	Return size of this matrix
		 * */
		inline unsigned Size() const{
			return this->Width()*this->Length();
		};
	
		// operator overloads	
		virtual Matrix operator + (Matrix x);	// smart operators
		virtual Matrix operator - (Matrix x);
		virtual Matrix operator * (Matrix x);
		virtual bool operator == (Matrix x);

		// scalar multiplication
		friend Matrix operator * (Atom& ax, Matrix& x) ;
		friend Matrix operator * (Matrix& x, Atom& ax) ;

		// output stream 
		/*	Year 2020
		 *	William Doyle
		 *	outstream operator overload for Matrix class
		 *	put values into stream in a nicly ordered fasion
		 *	(matrix layout and readable)
		 * */	
		friend std::ostream &operator<< (std::ostream &output, const Matrix &M) {
			double largest = 0;
			for (auto vec : M.matrix_members)
				for (Atom _atom: vec) 
					if (_atom.value > largest)
						largest = _atom.value;

			output << "\n";	
			// put the atoms from the matrix into the output stream	
			int _wdt = std::to_string((int)largest).length()+1 ;
			for (auto vec : M.matrix_members){
				for (Atom _atom : vec ) {
					output  << std::setw(_wdt) << _atom.value  << " ";
				}
				output << "\n";
			}
			output << "\n";
			return output;
		};
};
		
//scalar multiplication
Matrix operator * (Atom& ax, Matrix& x) ;
Matrix operator * (Matrix& x, Atom& ax) ;

/*	slow_add()
 *	December 5th 2020
 *	Simple add function.
 *	Wdd
 * */
inline Matrix slow_add(Matrix* _this, Matrix x){
//	std::cout << __func__ << "\n";
	Matrix rm = Matrix(_this->Width(), _this->Length());	// return matrix
	for (unsigned i = 0; i < _this->Width(); i++){
		for (unsigned k = 0; k < _this->Length(); k++){
			rm.WriteAt(i, k, _this->ReadAt(i, k)->value + x.ReadAt(i, k)->value );
		}
	}
	return rm;
}

/*	slow_subtract()
 *	December 5th 2020
 *	Simple subtraction function.
 *	Wdd
 * */
inline Matrix slow_subtract(Matrix* _this, Matrix x){
//	std::cout << __func__ << "\n";
	Matrix rm = Matrix(_this->Width(), _this->Length());	// return matrix
	for (unsigned i = 0; i < _this->Width(); i++){
		for (unsigned k = 0; k < _this->Length(); k++){
			//rm.WriteAt(i, k, 	_this->ReadAt(i, k)->value	-	x.ReadAt(i, k)->value);
			*( rm.Access(i, k) ) = *( _this->Access(i, k)) - *(x.Access(i, k));
		}
	}
	return rm;
}


/*	slow_mult()
 *	December 5th 2020
 *	Simple subtraction function.
 *	note that this function 	IS NOT RESPONSABLE for ensuring a.col count is b.row count.
 *	the SmarMult class does that
 *
 *	Wdd
 * */
inline Matrix slow_mult(Matrix* _this, Matrix x){
	
	Matrix a = *_this;
	Matrix b = x;

	if (a.Width() <= b.Width()){
//		std::cout << "Swap needed. Doing this a lot is a waste of time\n";
		a = x;
		b = *_this;
	}

	Matrix c = Matrix (b.Width(), a.Length());
	
	for (int i = 0; i < b.Width(); i++){
		for (int j = 0; j  < a.Length() ; j++){
			double sum = 0.0;
			for (int k = 0; k < a.Width(); k++){
				sum += a.ReadAt(k,i)->value * b.ReadAt(j,k)->value;
			}
			if ( c.WriteAt(j,i, sum) == false){	// write to the result matrix
				std::cerr << "Error in \'" << __func__ << "\', Failed to write to c at " << j << ", " << i << "\n";

			}
		}
	}

	return c;
}

/**
 *
 *
 * */
Matrix NaiveMultiply(Matrix* , Matrix* );
