/*

	November 20th 2020
	Tests the libery
	WDD
*/

#include "Matrix.hpp"
#include "GreyScaleImage.hpp"
#include "Kernel.hpp"
#include <ostream>
#include <sstream>
#include <chrono>	// used for testing time improvments

// add ncurses to test output to show mad skills (green passed, red failed. keep it simple)

#define ATOM_RANGE 16
//#define DO_ALL_TESTS
#ifdef DO_ALL_TESTS
	#define DO_KNOWN_PASSES
	#define DO_SMALL_MULT_TESTS
#endif
int main () {
	std::cout << "Tests for the matrix math system.\n";
	srand(time(NULL));
	const int size = 1001;
#ifdef DO_KNOWN_PASSES
	std::cout << "\nTest Suit One: Addition\n";
	{
		std::cout << "Test A. Small Matrix Addition: ";		
		Matrix m1 = Matrix(4, 4);
		Matrix m2 = Matrix(4, 4);
		for (unsigned i = 0; i < m1.Width(); i++){
			for (unsigned k = 0; k < m1.Length(); k++){
				m1.WriteAt(i, k, rand()%ATOM_RANGE);
				m2.WriteAt(i, k, rand()%ATOM_RANGE);
			}	
		}	
		Matrix m3 = m1+m2;
		if (m3.GetSum() == m1.GetSum() + m2.GetSum())
			std::cout << "passed.\n";
		else 
			std::cout << "failed.\n";

		std::cout << "Test B. Large Matrix Addition: ";		
		m1 = Matrix(size, size);
		m2 = Matrix(size, size);
		for (unsigned i = 0; i < m1.Width(); i++){		// populate matricies
			for (unsigned k = 0; k < m1.Length(); k++){
				m1.WriteAt(i, k, rand()%ATOM_RANGE);
				m2.WriteAt(i, k, rand()%ATOM_RANGE);
			}	
		}
		m3 = m1+m2;		// what we are actually trying to test
		if (m3.GetSum() == m1.GetSum() + m2.GetSum())
			std::cout << "passed.\n";
		else{ 
			std::cout << "failed.\n";
			std::cout << m3 <<"\n";

		}

		std::cout << "Test C. Commutative Property of Addition: ";
		if ((m1+m2).GetSum() == (m2+m1).GetSum())
			std::cout << "passed.\n";
		else 
			std::cout << "failed.\n";


	}
	std::cout << "\nTest Suit Two: Subtraction\n";
	{
		std::cout << "Test A. Small Matrix Subtraction: ";
		Matrix m1 = Matrix(4, 4);
		Matrix m2 = Matrix(4, 4);
		for (unsigned i = 0; i < m1.Width(); i++){
			for (unsigned k = 0; k < m1.Length(); k++){
				m1.WriteAt(i, k, rand()%ATOM_RANGE);
				m2.WriteAt(i, k, rand()%ATOM_RANGE);
			}	
		}	
		Matrix m3 = m1-m2;
		if (m3.GetSum() == m1.GetSum() - m2.GetSum())
			std::cout << "passed.\n";
		else 
			std::cout << "failed.\n";
		
		std::cout << "Test B. Large Matrix Subtraction: ";
		m1 = Matrix(size, size);
		m2 = Matrix(size, size);
		for (unsigned i = 0; i < m1.Width(); i++){		// populate matricies
			for (unsigned k = 0; k < m1.Length(); k++){
				m1.WriteAt(i, k, rand()%ATOM_RANGE);
				m2.WriteAt(i, k, rand()%ATOM_RANGE);
			}	
		}
		m3 = m1-m2;		// what we are actually trying to test
		if (m3.GetSum() == m1.GetSum() - m2.GetSum())
			std::cout << "passed.\n";
		else 
			std::cout << "failed.\n";

		std::cout << "Test C. Non Commutative Property of Subtraction: ";
		if ((m1-m2).GetSum() != (m2-m1).GetSum())
			std::cout << "passed.\n";
		else 
			std::cout << "failed.\n";
	}
#endif
#ifdef DO_SMALL_MULT_TESTS
	std::cout << "\nTest Suit Three: Multiplication\n";
	{
		std::cout << "Test A. Small Matrix Multiplication: \n source: https://www.mathsisfun.com/algebra/matrix-multiplying.html \n";
		Matrix m2 = Matrix(3, 2);
		Matrix m1 = Matrix(2, 3);

		m1.WriteAt(0,0, Atom(1));
		m1.WriteAt(0,1, Atom(2));
		m1.WriteAt(0,2, Atom(3));
		
		m1.WriteAt(1,0, Atom(4));
		m1.WriteAt(1,1, Atom(5));
		m1.WriteAt(1,2, Atom(6));

		std::cout << "m1\n" << m1 << "\n";

		m2.WriteAt(0, 0, Atom(7));
		m2.WriteAt(0, 1, Atom(8));
		m2.WriteAt(2, 0, Atom(11));
		
		m2.WriteAt(1, 0, Atom(9));
		m2.WriteAt(1, 1, Atom(10));
		m2.WriteAt(2, 1, Atom(12));
		
		std::cout << "m2\n" << m2 << "\n";
		
		Matrix m3 = m1*m2;
	
		std::cout << " m1: \n" << m1 << "\n";
		std::cout << " m2: \n" << m2 << "\n";
		std::cout << "!m3: \n" << m3 << "\n";
		std::cout << "check: \n" << m2*m1 << "\n";
		
		
		std::cout << "Test A2: Different small matrix multiplication: \n";

		{
			std::cout << "based on example found at https://www.onlinemathlearning.com/matrix-multiplication.html\n";
			Matrix mx1 = Matrix(2,3);
			Matrix mx2 = Matrix(3,2);
			
			mx1.WriteAt(0,0, Atom(1));
			mx1.WriteAt(0,1, Atom(2));
			mx1.WriteAt(0,2, Atom(-1));
			
			mx1.WriteAt(1,0, Atom(2));
			mx1.WriteAt(1,1, Atom(0));
			mx1.WriteAt(1,2, Atom(1));

			std::cout << "mx1\n" << mx1 << "\n";

			mx2.WriteAt(0, 0, Atom(3));
			mx2.WriteAt(0, 1, Atom(1));
			mx2.WriteAt(2, 0, Atom(-2));
			
			mx2.WriteAt(1, 0, Atom(0));
			mx2.WriteAt(1, 1, Atom(-1));
			mx2.WriteAt(2, 1, Atom(3));

			std::cout << "mx2\n" << mx2 << "\n";

			Matrix mx3 = mx1*mx2;
			std::cout << "mx3(Result):\n" << mx3 << "\n";
			
			Matrix mx4 = mx2*mx1;
			std::cout << "mx4(Commutability check Result):\n" << mx4 << "\n";


		}
	
		std::cout << "Test A3: Different small matrix multiplication: \n";

		{
			Matrix mx1 = Matrix(2,3);
			Matrix mx2 = Matrix(3,2);
			
			mx1.WriteAt(0,0, Atom(rand()%10));
			mx1.WriteAt(0,1, Atom(rand()%11));
			mx1.WriteAt(0,2, Atom(rand()%12));
			
			mx1.WriteAt(1,0, Atom(rand()%122));
			mx1.WriteAt(1,1, Atom(rand()%2));
			mx1.WriteAt(1,2, Atom(rand()%30));

			std::cout << "mx1\n" << mx1 << "\n";

			mx2.WriteAt(0, 0, Atom(rand()%3));
			mx2.WriteAt(0, 1, Atom(rand()%2));
			mx2.WriteAt(2, 0, Atom(rand()%4));
			
			mx2.WriteAt(1, 0, Atom(rand()%100));
			mx2.WriteAt(1, 1, Atom(rand()%6));
			mx2.WriteAt(2, 1, Atom(rand()%33));

			std::cout << "mx2\n" << mx2 << "\n";

			Matrix mx3 = mx1*mx2;
			std::cout << "mx3(Result):\n" << mx3 << "\n";


		}

		std::cout << "Test B. Random Matrix Multiplication: \n\n";	
		m1 = Matrix(10, 5);
		m2 = Matrix(5, 10);
		for (unsigned i = 0; i < m1.Width(); i++){		// populate matricies
			for (unsigned k = 0; k < m1.Length(); k++){
				m1.WriteAt(k, i, rand()%ATOM_RANGE);
				m2.WriteAt(i, k, rand()%ATOM_RANGE);
			}	
		}
		
		std::cout << "\tmatrix 1: \n" <<  m1 << "\n";
		std::cout << "\tmatrix 2: \n" <<  m2 << "\n";
//		m3 = m2*m1;		
		m3 = m1*m2;		
		std::cout << "\tmatrix 3 ('matrix 1' x 'mattrix 2'): \n" <<  m3 << "\n";
/*		for (int i = 0; i < m1.Width(); i++)
			for (int k = 0; k < m1.Length(); k++)
				// check all of the work. if anything is wrong say "failed" and goto fail point 2
				if (( (m1.ReadAt(i,k)->value)*(m2.ReadAt(i,k)->value) ) == m3.ReadAt(i,k)->value)
					continue;
				else{ 
					std::cout << "failed. (" << i << ", " << k << ")\n";
					goto FAILURE_REENTRY_POINT2; // is this worth it? Consider when cleaning
				}
		std::cout << "passed.\n";
	FAILURE_REENTRY_POINT2:

	*/
		std::cout << "Test C. Commutative Property of Multiplication: ";
		if ((m1*m2).GetSum() == (m2*m1).GetSum())
			std::cout << "passed.\n";
		else 
			std::cout << "failed. difference ('error') indicator:" << (m1*m2).GetSum() - (m2*m1).GetSum() << "\n";

		std::cout << "\n";


#endif
		std::cout << "Test D: Fast Multiplication\n";
		{
			Matrix bigA = Matrix(10, 5);
			Matrix bigB = Matrix(5, 10);

			for (unsigned i = 0; i < bigA.Width(); i++){		// populate matricies
				for (unsigned k = 0; k < bigB.Length(); k++){
					bigA.WriteAt(k, i, rand()%ATOM_RANGE);
					bigB.WriteAt(i, k, rand()%ATOM_RANGE);
				}	
			}
		
			std::cout << "Big A:\n" << bigA << "\n";			// show the matrices to the user	
			std::cout << "Big B:\n" << bigB << "\n";	

			Matrix bigC = bigA*bigB;
			
			std::cout << "Big C (solution):\n" << bigC << "\n";


			Matrix comp = slow_mult(&bigA, bigB);
			std::cout << "Slow Mult comparison:\n" << comp << "\n";
			
			comp = slow_mult(&bigB, bigA); // looks like slow multiplication is broken 
			std::cout << "(reality check) Slow Mult comparison:\n" << comp << "\n";
			


		}




	}

/*
	// create two matrix's 
	Matrix m1 = Matrix(3, 4);
	Matrix m2 = Matrix(3, 4);
	
	// populate matrix one
	for (unsigned i = 0; i < m1.Width(); i++){
		for (unsigned k = 0; k < m1.Length(); k++){
			m1.WriteAt(i, k, rand()%ATOM_RANGE);
		}	
	}	

	// populate matrix two
	for (unsigned i = 0; i < m2.Width(); i++){
		for (unsigned k = 0; k < m2.Length(); k++){
			m2.WriteAt(i, k, rand()%ATOM_RANGE);
		}	
	}	
	std::cout << "Matrix One: \n";
	std::cout << m1;

	std::cout << "Matrix Two: \n";
	std::cout << m2;
	
	std::cout << "Matrix One + Two: \n";
       	std::cout << m1+m2;	
	
	std::cout << "Matrix One - Two: \n";
       	std::cout << m1-m2;	
	
	Atom n1 = 2;	
	std::cout << "Matrix One * " << n1 << ": \n";
       	std::cout << m1*n1;	

	n1 = 3;
	std::cout << "Matrix Two * " << n1 << ": \n";
       	std::cout << m2*n1;	

	std::cout << n1 << " * Matrix Two (should be same as above): \n";
       	std::cout << n1*m2;

	std::cout << " m1 times m2 : \n";
//	std::cout << m1*m2;
	Matrix mA = Matrix (10, 10);
	Matrix mB = Matrix (10, 10);
	for (int i = 0; i < 10; i++)
		for (int k = 0; k < 10; k++){
			mA.WriteAt(i, k, rand()%3);
			mB.WriteAt(i, k, rand()%3);

		}

	std::cout << "mA: \n";
	std::cout << mA;
	std::cout << "mB: \n";
	std::cout << mB;

	std::cout << " mA times mB : \n";
	Matrix mC = mA*mB;
	std::cout << mC;



	GreyScaleImage* image = new GreyScaleImage("images/img.ppm");

	

 


#define ADDITION_TEST
#ifdef ADDITION_TEST
	const unsigned __w = 100;
	const unsigned __l = 100;
	std::cout << "Addition Test\n";
	Matrix mtx1 = Matrix(__w, __l);
	Matrix mtx2 = Matrix(__w, __l);
	// populate with random numbers
	for (int i = 0; i < __w; i++){
		for (int k = 0; k < __l; k++){
			mtx1.WriteAt(i, k, 1);
			mtx2.WriteAt(i, k, 2);
		}
	}

//	std::cout << mtx1 << "\n\n";
//	std::cout << mtx2 << "\n\n";
	Matrix mtx3;
	{
		auto startTime = std::chrono::high_resolution_clock::now();
		mtx3 = mtx1+mtx2;
		auto stopTime = std::chrono::high_resolution_clock::now();
		auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stopTime-startTime);
		
		std::cout << "Addition took " << duration.count()<< " microseconds.\n";
	}
	if (mtx3.GetSum() == (mtx1.GetSum() + mtx2.GetSum()) ){
		std::cout << "Calculation 1 passed equivilency check.\n";
	}
	else {
		std::cout << "Calculation 1 failed equivilency check. \n";
		std::cout << "diff " << mtx3.GetSum() - (mtx1.GetSum() + mtx2.GetSum()) << "\n";
	}

	Matrix mtx4;
	{
		auto startTime = std::chrono::high_resolution_clock::now();
		mtx4 = mtx3-mtx2;
		auto stopTime = std::chrono::high_resolution_clock::now();
		auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stopTime-startTime);
		
		std::cout << "Subtraction took " << duration.count()<< " microseconds.\n";

	}

	if (mtx4.GetSum() == mtx1.GetSum()  ){
		std::cout << "Calculation 2 passed equivilency check.\n";
	}
	else {
		std::cout << "Calculation 2 failed equivilency check. \n";
		std::cout << "diff " << mtx4.GetSum() - mtx1.GetSum()  << "\n";
	}


//	std::cout << mtx3 << "\n\n";

#endif

#define CONVOLUTION_TEST
#ifdef CONVOLUTION_TEST
	auto startTime = std::chrono::high_resolution_clock::now();
	GreyScaleImage image3 = image->Convolution(Kernel::EdgeDetect(1), 8);
	auto stopTime = std::chrono::high_resolution_clock::now();
	auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stopTime-startTime);
	std::cout << "Convolution took " << duration.count()<< " milliseconds.\n";
	image3.Save("images/output/edgeDetectDemo.ppm");
#endif
	delete image;
//	GreyScaleImage gsi1 = image->Convolution(Kernel::EdgeDetect(1));
	GreyScaleImage gsi2 = image->Convolution(Kernel::Blur()).Convolution(Kernel::Blur()).Convolution(Kernel::EdgeDetect(1));
	GreyScaleImage gsi3 = *(image)+gsi2;
//	gsi3 = gsi3-*(image);
	gsi3.Save("img_c.ppm");
*/

};
