Matrix Math Library Commenting Style Guide

When to comment:
	A comment is required at the top of each source file, the top of each function, and spairingly throughout the document to clarify blocks of code. Code should be fairly self documenting.

What to comment:
	Top of file
		Date of creation
		file name
		author(s)
		Description of the files contents.
	
