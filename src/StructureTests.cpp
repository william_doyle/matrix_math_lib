/*
 *	filename	StructureTests.cpp
 *	purpose		demonstrate the structural consistency of this library.
 *				demonstrate the relationships between our classes
 *	date		december 28th 2020
 *	author		William Doyle
 *
 *
 * */


/* includes */
#include "Atom.hpp"
#include <iostream>


int main (void) {
	Atom* atom = new Atom(1.23); 

	std::cout << *atom << "\n";

	delete atom;

	return 1;
};



