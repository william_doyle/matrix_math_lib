#include "Matrix.hpp"
#include "GreyScaleImage.hpp"
#include "Kernel.hpp"
#include <assert.h>

/*
 *	wdd
 *	january 1st 2021
 *
 * */
int main() {
	
	Matrix* m1 = new Matrix(2, 2);
	*(m1->Access(0,0)) = 1;
	*(m1->Access(0,1)) = 2;
	*(m1->Access(1,0)) = 3;
	*(m1->Access(1,1)) = 4;

	Matrix* m2 = new Matrix(2, 2);
	*(m2->Access(0,0)) = 2;
	*(m2->Access(0,1)) = 0;
	*(m2->Access(1,0)) = 1;
	*(m2->Access(1,1)) = 2;
	
	std::cout << NaiveMultiply(m1, m2) << "\n\n\n" << NaiveMultiply(m2, m1) << "\n\n";


	assert( *m1 + *m2 - *m1 == *m2);
	assert(*m1 * *m2 == NaiveMultiply(m2, m1));

	delete m1;	
	delete m2;	
	
	return 1;
}
