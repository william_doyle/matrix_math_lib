#include "GreyScaleImage.hpp"
#include "Kernel.hpp"

/*
 *	image_processor.cpp
 *	William Doyle
 *	December 6th 2020
 *
 * */

inline std::string help() {

	return "not enough arguments: file in\t\tfile out\tinstruction strings (two char strings used to specify kernels to apply to the images)\n";
};

int main (int argc, char** argv){
	std::cout << "imgpro | William Doyle | 2020 \n";
	/* file in 	file out	instruction string */
	if (argc < 4){
		std::cout << help() <<"\n";
		return 0;
	}
	
	srand(time(NULL));

	std::string fin = argv[1];
	std::string fout = argv[2];
	int thrdcont = 7;

	GreyScaleImage gsi = GreyScaleImage(fin);

	std::vector<std::string> tasks;
	for (int i = 3; i < argc; i++)
		tasks.push_back(argv[i]);

	for (std::string s : tasks){
		//	E D G E   D E T E C T I O N 
		if (s.at(0) == 'e'){
			if (s.at(1) == '0')
				gsi = gsi.Convolution(Kernel::EdgeDetect(0), thrdcont); // apply ed1
			else if (s.at(1) == '1')
				gsi = gsi.Convolution(Kernel::EdgeDetect(1), thrdcont); // apply ed1
			else if (s.at(1) == '2')
				gsi = gsi.Convolution(Kernel::EdgeDetect(2), thrdcont); // apply ed1
		}
		
		//	B L U R 
		else if (s.at(0) == 'b')
			gsi = gsi.Convolution(Kernel::Blur(), thrdcont);
		//	S H A R P E N 
		else if (s.at(0) == 's')
			gsi = gsi.Convolution(Kernel::Sharpen(), thrdcont);
		//	R A N D O M 
		else if (s.at(0) == 'r'){
			Matrix randomkernel = Kernel::Random(-2, 2);
			std::cout << randomkernel << "\n";
			gsi = gsi.Convolution(randomkernel, thrdcont);
		}


	}
		
	gsi.Save(fout);


};
