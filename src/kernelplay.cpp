#include "GreyScaleImage.hpp"
#include "Kernel.hpp"
#include "ColorImage.hpp"



// January 2nd 2020
// william Doyle
// playing with kernels



#define THREAD_AMOUNT 9
#define BLUR_AMOUNT 3
int main () {
	
	ColorImage* ci = new ColorImage("../images/wl_zbr_comp.ppm");

	GreyScaleImage* gsi = new GreyScaleImage(*ci);
	std::cout << "Ci created\n";
	
	for (int i = 0; i < BLUR_AMOUNT; i++){
		*gsi = gsi->Convolution(Kernel::Blur(), THREAD_AMOUNT);
		std::cout << "Blured GreyScaleImage " << i+1 << " times.\n";
	}

	gsi->Save("../images/blury_zbra_comp.ppm");
	*gsi = gsi->Convolution(Kernel::EdgeDetect(2), THREAD_AMOUNT);
	std::cout << "GreyScaleImage: Edge's detected\n";
	gsi->Save("../images/gs_zbra_comp3.ppm");



	for (int i = 0; i < BLUR_AMOUNT; i++){
		*ci = ci->Convolution(Kernel::Blur(), THREAD_AMOUNT);
		std::cout << "Blured ColorImage " << i+1 << " times.\n";
	}

	ci->Save("../images/blurycolor_zbra_comp.ppm");
	*ci = ci->Convolution(Kernel::EdgeDetect(2), THREAD_AMOUNT);
	std::cout << "ColorImage: Edge's detected\n";
	ci->Save("../images/color_zbra_ed_comp.ppm");

	delete gsi;
	delete ci;
	
/*
	// generate the image (this is the initial conditions)
	srand(time(NULL));

	GreyScaleImage gsi_m = GreyScaleImage("../images/woods_lores_color.ppm");

	Matrix ker = Kernel::Blur();//Random(-1, 1.5);
	std::cout << ker << "\n";

	gsi_m = gsi_m.Convolution(ker, THREAD_AMOUNT).Convolution(Kernel::EdgeDetect(1), 2);

//	std::stringstream ss;
//	ss << "#convolution preformed with following kernel\n" << ker.generate_ppm_comment();
//	GreyScaleImage(gsi_m).Save("../images/grey_version.ppm", ss.str());
	GreyScaleImage(gsi_m).Save("../images/samesrc/grey_version.ppm");
	*/
	return 1;
};
