#include "ColorImage.hpp"
#include "Atom.hpp"
#include <fstream>
#include "GreyScaleImage.hpp"

/*	January 6th 2021
 *	William Doyle
 *	ColorImage.cpp
 *
 *
 *
 * */

/*
 *	January 6th 2021
 *	William Doyle
 *	ColorImage constructor from file name
 *
 * */
ColorImage::ColorImage(std::string filename){

	std::ifstream file(filename);
	std::string sline;
	unsigned count = 0;

	while(std::getline(file, sline)){
		if (sline.at(0) == '#')			// ignore comments
			continue;
		if (count == 0){				
			this->format = sline;
			count++;
			continue;
		}	
		else if (count == 1){
			this->number_cols = std::stoi(sline.substr(0, sline.find(" ")));
			this->number_rows = std::stoi(sline.substr(sline.find(" "), sline.size() - sline.find(" ")));
			count++;
			continue;
		}
		else if (count == 2){
			this->max_color_value = std::stoi(sline);
			count++;
			continue;
		}
		else {
			std::vector<Atom> rvalues = std::vector<Atom>();
			std::vector<Atom> gvalues = std::vector<Atom>();
			std::vector<Atom> bvalues = std::vector<Atom>();
		
			this->reds   = std::shared_ptr<Matrix>( new Matrix(this->number_rows, this->number_cols));
			this->greens = std::shared_ptr<Matrix>( new Matrix(this->number_rows, this->number_cols));
			this->blues  = std::shared_ptr<Matrix>( new Matrix(this->number_rows, this->number_cols));

			// get values from image file and place into respective vectors
			unsigned index = 0;
			do{	   
				if (index %3 == 0)
					rvalues.push_back(std::stoi(sline));
				else if (index %3 == 1)
					gvalues.push_back(std::stoi(sline));
				else if (index %3 == 2)
					bvalues.push_back(std::stoi(sline));
				index++;
			}while (std::getline(file, sline));

			// put values into correct matrix
			for (unsigned i = 0; i < this->reds->Width(); i++){
				for (unsigned k = 0; k < this->reds->Length()-1; k++){
						*(reds->Access(i, k)) 	= rvalues.at((i * this->reds->Length()) + k);
						*(greens->Access(i, k))	= gvalues.at((i * this->greens->Length()) + k);
						*(blues->Access(i, k)) 	= bvalues.at((i * this->blues->Length()) + k);
				}
			}
			break;
		}
	}
}

/*
 *	January 6th 2021
 *	William Doyle
 *	ColorImage destructor
 * */
ColorImage::~ColorImage(){
}

/*
 *	January 6th 2021
 *	William Doyle
 *	Save this image as a ppm file
*/
 void ColorImage::Save(std::string filename){
	 std::ofstream ofs;
	 ofs.open (filename, std::ios_base::out ); 
	 ofs << this->format << "\n";
	 
	 ofs << this->number_cols << ' '<< this->number_rows << std::endl << this->max_color_value << std::endl;	
	 for (unsigned i = 0; i < this->reds->Width(); i++) {
		 for (unsigned k = 0; k < this->reds->Length(); k++) {
			 if (ofs.is_open()){
				 ofs << *(this->reds->ReadAt(i, k)) << "\n";
				 ofs << *(this->greens->ReadAt(i, k)) << "\n";
				 ofs << *(this->blues->ReadAt(i, k)) << "\n";
			 }
			 else 
				 std::cerr << "Error: The File Became Closed!\n";
		 }
	 }

	 ofs.close();	 
}

/*
 *	william Doyle
 *	January 6th 2021
 *	Generate ColorImage from 3 component (R G B) matricies and an optional string for the formate (defaults to 'P3')
 *
 * */
ColorImage::ColorImage(Matrix r, Matrix g, Matrix b, std::string fmat){
	this->number_cols = r.Length();
	this->number_rows = r.Width();
	this->format = fmat;
	
	this->reds = std::shared_ptr<Matrix>(new Matrix());
	*(this->reds) = r;
	this->greens = std::shared_ptr<Matrix>(new Matrix());
	*(this->greens) = g;
	this->blues = std::shared_ptr<Matrix>(new Matrix());
	*(this->blues) = b;
	this->max_color_value = 255; // usually fine
}

/*
 *	William Doyle
 *	January 6th 2021
 *	Preform a convolution on this color image by convolving each component sepretly and
 *	building a new image with the results
 *
 * */
ColorImage ColorImage::Convolution(Matrix M, int nthreads){
	return ColorImage(
			this->reds->_convolution(M, nthreads), 
			this->greens->_convolution(M, nthreads), 
			this->blues->_convolution(M, nthreads),
			this->format
			);
}





