#include "Kernel.hpp"

/* William Doyle
 *	A collection of static functions for generating kernels
 *
 * */

/*
 *	William Doyle
 *	Random()
 *	Generate a random kernel
 *
 * */
Matrix Kernel::Random(int min , int max) {
	

	Matrix kern =  Matrix(3, 3);
	for (int i = 0; i < kern.Width(); i++)
		for (int k = 0; k < kern.Length(); k++){
			*(kern.Access(i, k)) = 	min + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(max-min)));
		}


	return kern;	


}



/*
 


   */
Matrix Kernel::Sharpen(){

	Matrix kern =  Matrix(3, 3);
	kern.WriteAt(0, 0, Atom(0)); 
	kern.WriteAt(0, 1, Atom(-1));
	kern.WriteAt(0, 2, Atom(-0));

	kern.WriteAt(1, 0, Atom(-1)); 
	kern.WriteAt(1, 1, Atom(5));
	kern.WriteAt(1, 2, Atom(-1));

	kern.WriteAt(2, 0, Atom(0)); 
	kern.WriteAt(2, 1, Atom(-1));
	kern.WriteAt(2, 2, Atom(0));

	return kern;
};

/*




   */
Matrix Kernel::Blur(){
	Matrix blur = Matrix(3, 3);
	// row 1
	blur.WriteAt(0, 0, Atom (1.0/16.0));
	blur.WriteAt(0, 1, Atom( 2.0/16.0));
	blur.WriteAt(0, 2, Atom(1.0/16.0));


	// row 2
	blur.WriteAt(1, 0, Atom( 2.0/16.0));
	blur.WriteAt(1, 1, Atom( 4.0/16.0));
	blur.WriteAt(1, 2, Atom( 2.0/16.0));
	// row 3
	blur.WriteAt(2, 0, Atom( 1.0/16.0));
	blur.WriteAt(2, 1, Atom( 2.0/16.0));
	blur.WriteAt(2, 2, Atom( 1.0/16.0));
	return blur;
};

/**
 *	November 25th 2020
 *	Wdd
 *	EdgeDetect()
 *	returns a kernel used for edge detection
 *
 * */
Matrix Kernel::EdgeDetect(short type_id ){
	Matrix kernel = Matrix(3, 3);
	if (type_id == 0){
		// row 1
		kernel.WriteAt(0, 0,  Atom( 1));
		kernel.WriteAt(0, 1,  (Atom) 0);
		kernel.WriteAt(0, 2, (Atom) -1);
		// row 2
		kernel.WriteAt(1, 0, (Atom) 0);
		kernel.WriteAt(1, 1, (Atom) 0);
		kernel.WriteAt(1, 2, (Atom) 0);
		// row 3
		kernel.WriteAt(2, 0, (Atom) -1);
		kernel.WriteAt(2, 1,  (Atom) 0);
		kernel.WriteAt(2, 2,  (Atom) 1);
		return kernel;
	}
	else if (type_id == 1){
		// works well on images that are of differing dimensions
		// row 1
		kernel.WriteAt(0, 0, (Atom)-1);
		kernel.WriteAt(0, 1, (Atom)-1);
		kernel.WriteAt(0, 2, (Atom)-1);
		// row 2
		kernel.WriteAt(1, 0, Atom( -1));
		kernel.WriteAt(1, 1, (Atom) 8);
		kernel.WriteAt(1, 2, (Atom) -1);
		// row 3
		kernel.WriteAt(2, 0, (Atom) -1);
		kernel.WriteAt(2, 1, (Atom) -1);
		kernel.WriteAt(2, 2, (Atom) -1);
		return kernel;
	}else if (type_id == 2){
		// row 1
		kernel.WriteAt(0, 0, Atom( 0));
		kernel.WriteAt(0, 1, (Atom) -1);
		kernel.WriteAt(0, 2, (Atom) 0);
		// row 2
		kernel.WriteAt(1, 0, (Atom) -1);
		kernel.WriteAt(1, 1, (Atom) 4);
		kernel.WriteAt(1, 2, (Atom) -1);
		// row 3
		kernel.WriteAt(2, 0, (Atom) 0);
		kernel.WriteAt(2, 1, (Atom) -1);
		kernel.WriteAt(2, 2, (Atom) 0);	
		
		return kernel;
	}

	return kernel;

};
