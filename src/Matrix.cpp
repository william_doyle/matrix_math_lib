#include "Matrix.hpp"
#include <sstream>
#include <assert.h>
#include <iostream>
#include "MultiThreadedMatrixOperation.hpp"
#include "SmartOperators.hpp"
#include <fstream>
#include <iostream>
#include "ConvolutionThreadSuitcase.hpp"
/*
	Simple width length constructor

*/
Matrix::Matrix(const unsigned _width, const unsigned _length){
	this->matrix_members = std::vector<std::vector<Atom>>(_width);
	for (unsigned i = 0; i < _width; i++) {
		this->matrix_members.at(i) = std::vector<Atom>();
		for (unsigned k = 0; k < _length; k++){
			this->matrix_members.at(i).push_back(Atom(0));
		}
	}
};

/*
 *	William Doyle
 *	Zero Arg constructor.
 * */
Matrix::Matrix() {
};

/*	William Doyle
 *	Destructor
 * */
Matrix::~Matrix() {
//	std::cout << __func__ << "\n";
};

/*
 * December 24th 2020
 * wdd
 * Constructor from two unsigneds and an array of doubles
 * */
Matrix::Matrix(const unsigned w, const unsigned l, double* values){
	// assert that values at w l is not out of bounds (if it is that's 100% the callers fault)	
//	assert(*(values+(sizeof(double*)*w+l)) == values[sizeof(values)/sizeof(double*)] );

	this->matrix_members = std::vector<std::vector<Atom>>(w);
	// traverse 'values' and populate 'this'
	double* cursor = values;
	for (unsigned i = 0; i < w; i++)
		for (unsigned k = 0; k < l; k++){
			this->matrix_members.at(i).push_back( Atom(*values) );
			values++;	// pointer arithmatic
		}
};

/*
 *	Wdd
 *	GetType()
 *
 * */
std::string Matrix::GetType() {
	std::stringstream ss;
	ss << this->Width() << " by " << this->Length() << " Matrix";
	return ss.str();
}

/*	wdd
 *	Establish()
 *	hacky gross thing I did. kinda like a constructor but gross
 * */
void Matrix::Establish(const unsigned _width, const unsigned _length){
	this->matrix_members = std::vector<std::vector<Atom>>(_width);
	for (unsigned i = 0; i < _width; i++) {
		this->matrix_members.at(i) = std::vector<Atom>();
		for (unsigned k = 0; k < _length; k++)
			this->matrix_members.at(i).push_back(Atom(0.0));
	}
}

/*
 * 	William Doyle
 *	Get Width of the matrix (for us the width also describes size of the parent vector)
 * */
unsigned Matrix::Width() const {
	return this->matrix_members.size();
}

/*	William Doyle
 *	Get the length of the matrix. We do this by returning the size of the first element in the parent vector. 
 *	*/
unsigned Matrix::Length() const {
//#define SAFE_LENGTH
#ifdef SAFE_LENGTH
	for (unsigned i = 0; i < this->Width(); i++){
		assert(this->matrix_members.at(i).size() == this->matrix_members.at(0).size());
	}
#endif
	return this->matrix_members.at(0).size(); // returns the size of the first sub vector and considers this the length
};

/*
 *	December 26th 2020
 *	William Doyle
 *	Access
 *		provide access (via a ptr) to a value in this matrix. Without any safty precautions. 
 * */
Atom_ptr Matrix::Access(const unsigned i, const unsigned k){
		return &(this->matrix_members.at(i).at(k));
}

/*	William Doyle
 *	Returns the atom pointer at the requested position. 
 * */
Atom_ptr Matrix::ReadAt(const unsigned _widthPOS, const unsigned _lengthPOS) {
	try {
		return &(this->matrix_members.at(_widthPOS).at(_lengthPOS));
	}catch (const std::out_of_range& oor) {
		std::cerr << "Out of range error from " << __func__ << "( " << _widthPOS << ", " << _lengthPOS << "). " << oor.what() << "\n";
	} catch (...) {
		std::cerr << "Unknown error occured in " << __func__ << "\n";
	}
	return nullptr;
}

/*
 *	William Doyle
 *	WriteAt
 *	HAS OVERHEAD DUE TO NUMEROUS THINGS INCLUDING A TRY/CATCH BLOCK
 * */
bool Matrix::WriteAt(const unsigned _widthPOS, const unsigned _lengthPOS, const Atom aValue){
	try {
		this->matrix_members.at(_widthPOS).at(_lengthPOS) = aValue ;
		if (this->matrix_members.at(_widthPOS).at(_lengthPOS).value != aValue.value ){
			std::cerr << "Write Unverified!\n";
			return false;
		}
	} catch (std::exception& e){
		std::cout << "Error in " << __func__  << e.what() << "\n"; // small but importent change
		return false;
	}catch (...) {
		// if things go bad for any reason
		std::cerr << __func__ << " write to matrix failed (default catch)\n";
		return false; // False indicates failure
	}
	return true;
}

/*
 *	GetSum()
 *	Gets the sum of this matrix (adds up all values in matrix)
 *	William Doyle
 *	// NOTICE RETURN TYPE CHANGED FROM INT TO ATOM
 *	feature request: make this multithreaded but only when it would be faster
 * */
Atom Matrix::GetSum() const {
	Atom sum;
	for (auto vec : this->matrix_members)
		for (Atom atom : vec)
			sum += atom;
	return sum;
}

/*	December 13th 2020
 *	wdd
 *	GetAverage()
 * */
Atom Matrix::GetAverage() const{
	return this->GetSum()/this->Size();
}

/*
 *	Addition operator
 *
 * */
Matrix Matrix::operator+(Matrix x ){
	return SmartAdd(*this, x).Do();
};

/*	Subtraction Operator
 *	December 5th 2020 (full rewrite)
 *	Wdd
 *
 * */
Matrix Matrix::operator-(Matrix x ){
	return SmartSubtract(*this, x).Do();
};

/**
 *	Multiplication overload for matrix class
 *
 *
 * */
Matrix Matrix::operator*(Matrix x) {
	std::cout << "stub here \n";
	return SmartMult(*this, x).Do();
};

/*
 *	December 13th 2020
 *	== operator overload implementation
 *	wdd
 *	source (for definition of matrix equivilence): https://www.wyzant.com/resources/lessons/math/precalculus/matrices/matrix_equality
 * */
bool Matrix::operator==(Matrix x) {
	// check that dimensions are exactly the same
	if ( (this->Width() != x.Width()) || (this->Length() != x.Length()) ){
		std::cout << __func__ << ": dimensions are not the same\n";
		return false; // these two can't be equivilent because they don't even have the same dimension sizes	
	}
	for (unsigned i = 0; i < this->Width(); i++)
		for (unsigned k = 0; k < this->Length(); k++)
			if (*(this->Access(i,k)) == *(x.Access(i,k)) )
				continue;
			else {
				std::cout << __func__ <<  ": value miss match at " << i << ", " << k << "\n";
				return false;
			}
			
	return true;
};

// scaler multiplication (matrix multiplied by an Atom)
Matrix operator*(Atom& ax , Matrix& x){
	
	//  operator overload
	Matrix rm = Matrix(x.Width(), x.Length());	// return matrix
	for (unsigned i = 0; i < x.Width(); i++){
		for (unsigned k = 0; k < x.Length(); k++){
			rm.WriteAt(i, k, Atom(x.ReadAt(i, k)->value * ax.value ));
		}
	}
	return rm;
}

// scaler multiplication (matrix multiplied by an Atom)
Matrix operator*(Matrix& x, Atom& ax){
	return ax*x;
}

/***
 *
 *	December 12th 2020
 *	William Doyle
 *	SubMatrixByRange
 *
 * */
Matrix Matrix::SubMatrixByRange(const unsigned max_x, const unsigned max_y, const unsigned min_x, const unsigned min_y){
	
	assert(max_x-min_x > 0);
	assert(max_y-min_y > 0);

	Matrix submatrix = Matrix(max_x-min_x, max_y-min_y);
	
	for (unsigned i = min_x; i < max_x; i++){
		for (unsigned k = min_y; k < max_y; k++){
			if (submatrix.WriteAt( i-min_x, k-min_y, *(this->ReadAt(i,k))) == false){ // important change since last push
				std::cerr << "Failed to write to submatrix at " << i << ", " << k << "! Submatrix has dimensions " << submatrix.Width() << " by " << submatrix.Length() << "\n";
			}
		}
	}

	return submatrix;
}

/**
 * December 17th 2020
 * William Doyle
 * SubMatrixByQuarter(int)
 * args: 1 int (the quarter of this matrix the caller wishes to be returned)
 * */
Matrix Matrix::SubMatrixByQuarter(const int q){
	switch (q){
		case 0:
			// first quarter top left
			return this->SubMatrixByRange( this->Width()/2, this->Length()/2, 0, 0 );
		case 1:
			return this->SubMatrixByRange(this->Width()/2, this->Length(), 0, this->Length()/2  );
		case 2:
			// bottom left
			return this->SubMatrixByRange(this->Width(), this->Length()/2, this->Width()/2, 0  );
		case 3:
			return this->SubMatrixByRange(this->Width(), this->Length(), this->Width()/2, this->Length()/2  );
	};
	std::cerr << "Error: " << __func__ << " does not understand input " << q << ".\n"; 
	return Matrix(0,0);	
};

/**
 *	December 18th 2020
 *	Wdd
 *	FromCSVFile
 *	args: 1 (string)[the name of the file holding matrix values in CSV format]
 *	returns matrix of values found in the file
 * */
Matrix Matrix::FromCsvFile(const std::string filename){
	std::vector<std::vector<std::string>> map;
	std::fstream fCsv;
	fCsv.open(filename, std::ios::in);
	if (fCsv.is_open()) {
		std::string line;
		while (std::getline(fCsv, line)){
			std::stringstream ss(line);
			std::string s;
			std::vector<std::string> vS =  std::vector<std::string>();
			while (std::getline(ss, s, ',')) {
				vS.push_back(s);
			}
			map.push_back(vS);
		}
	}else {
		std::cerr << "File input not working!\n";
	}
	fCsv.close();
	Matrix result = Matrix (map.size(), map.at(0).size());
	for (int i = 0 ; i < result.Width(); i++){
		for (int k = 0 ; k < result.Length(); k++){
			if (result.WriteAt(i, k, std::stod(map.at(i).at(k))) == false)
				std::cerr << __func__ << " error!\n";
		}
	}
	return result;
};

/**
 *		December 18th 2020
 *		wdd
 *		SaveAsCsvFile()
 *		This is not a static method
 *		CSV: Comma Seperated values
 *
 * */
void Matrix::SaveAsCsvFile(const std::string filename) {
	std::ofstream fs;
	fs.open(filename);
	for (int i = 0; i < this->Width(); i++){
		for (int k = 0; k < this->Length(); k++){
			fs << this->ReadAt(i,k)->value ;
			
			if (k != this->Length()-1)
				fs << ",";
		}
		fs << "\n";
	}
	fs.close();	
};

/*
 *	wdd
 *	December 18th 2020
 *	Glue
 *	Attach a matrix to this matrix and return the result
 *
 * */
Matrix Matrix::Glue(const std::string position, const Matrix mGuest){
	
	unsigned ___length = 0;
	unsigned ___width = 0;
	Matrix mFirst, mSecond;
	
/*
 	chose a 'first' and a 'second' matrix
	determine the correct values for mReslut width and length

*/   
	if ((position == "above")||(position == "below")){
		if (this->Width() != mGuest.Width()){
			std::cerr << __func__ << "These matricies won't line up on that (up down) axis\n";
			return Matrix(0,0);
		}

		___length = this->Length()+ mGuest.Length();
		___width = this->Width();

		mFirst = (position == "above")? mGuest:*this ;
		mSecond = (position == "below")? mGuest:*this ;
	}
	else if ((position == "left")||(position == "right")){
		if (this->Length() != mGuest.Length()){
			std::cerr << __func__ << "These matricies won't line up on that (left right) axis\n";
			return Matrix(0,0);
		}

		___width = this->Width() + mGuest.Width();
		___length = this->Length();
		
		mFirst = (position == "left")? mGuest:*this ;
		mSecond = (position == "right")? mGuest:*this ;
	}
	
/*
 	use determined values to construct mResult
*/   
	Matrix mResult = Matrix( ___width, ___length);

/*
 	copy values from 'mFirst' into the first half of 'mResult'. Copy 'mSecond' into the second half of mResult
*/   
	if (mResult.Width() == this->Width()){
		for (unsigned i = 0; i < mResult.Width(); i++)
			for (unsigned k = 0; k < mResult.Length(); k++)
				if (k < mFirst.Length())
					mResult.WriteAt(i, k, mFirst.ReadAt(i,k)->value);
				else 
					mResult.WriteAt(i, k, mSecond.ReadAt(i,k-mFirst.Length())->value);
	}
	else if (mResult.Length() == this->Length())	{
		for (unsigned i = 0; i < mResult.Width(); i++)
			for (unsigned k = 0; k < mResult.Length(); k++)
				if (i < mFirst.Width())
					mResult.WriteAt(i, k, mFirst.ReadAt(i,k)->value);
				else 
					mResult.WriteAt(i, k, mSecond.ReadAt(i-mFirst.Width(),k)->value);
	}

	return mResult;
};

/*	December 22nd 2020
 *	William Doyle
 *	PopulateRandomValuesInRange(int mini, int maxi)
 *
 *
 * */
void Matrix::PopulateRandomValuesInRange(const int mini, const int maxi){
	for (int i = 0; i < this->Width(); i++)
		for (int k = 0; k < this->Length(); k++)
			//this->WriteAt(i, k, rand() %(maxi + 1 - mini) + mini);	
			*(this->Access(i, k)) = Atom( rand() %(maxi + 1 - mini) + mini );	
};

/*	December 22nd 2020
 *	William Doyle
 *	copy() 
 *	return a pointer to a copy of this matrix
 *	REMEMBER TO CALL DELETE 
 *
 * */
Matrix* Matrix::copy(){
	Matrix* result = new Matrix(this->Width(), this->Length());
	for (unsigned i = 0; i < this->Width(); i++)
		for (unsigned k = 0; k < this->Length(); k++)
			*(result->Access(i,k)) = *(this->Access(i,k));
			//result->matrix_members.at(i).at(k) = this->matrix_members.at(i).at(k);
	return result;
};

/*
 *		WDD
 *		December 15th 2020 (uninlined and moved from .hpp to .cpp)
 *		Max()
 *		Return the largest value in this matrix
 * */
Atom Matrix::Max(){
	Atom largest = Atom(0.0);
	for (auto vec : this->matrix_members)
		for (Atom _atom: vec) 
			if (_atom > largest)
				largest = _atom;
	return largest;
};

/*
 *		WDD
 *		December 26th 2020
 *		return the smallest atom found
 * */
Atom Matrix::SmallestValue(){
	Atom smallest = *(this->Access(0,0));
	for (auto vec : this->matrix_members)
		for (Atom _atom: vec) 
			if (_atom < smallest)
				smallest = _atom;
	return smallest;
};

/*
 *	wdd 
 *	edited heavily on 2nd january 2021
 *
 * */
Matrix NaiveMultiply(Matrix* A, Matrix* B){
	//	A is n x m
	//	B is m x p
	//	C is n x p
	
	unsigned n , m, p;
	
	if (A->Width() == B->Length()){
		m = A->Width();
		n = A->Length();
		p = B->Width();
	}
	else if (A->Length() == B->Width()){
		m = A->Length();
		n = A->Width();
		p = B->Length();
	}
	else {
		std::cout << "bad\n";
		exit(1);
	}

	Matrix result = Matrix(n, p);

	for (unsigned i = 0; i < n; i++){
		for (unsigned j = 0; j < p; j++){
			*(result.Access(i, j)) = Atom(0.0);
			for (unsigned k = 0; k < m; k++){
				*(result.Access(i, j)) += *(A->Access(i, k)) *  *(B->Access(k, j));
			}
		}	
	}

	return result;
};

/*
 *	January 6th 2021
 *	william doyle
 *	generate a a string from this matrix that will fit nicly into a ppm file as a comment.
 *	generate_ppm_comment()
 *
 * */
std::string Matrix::generate_ppm_comment(){
	
	std::stringstream ss;
	ss << "#" << *this;
	std::string s = ss.str();

//	boost::replace_all(s, "\n", "\n#"); // not worth installing boost for
	
	size_t pos = s.find("\n");
	while (pos != std::string::npos){
		s.replace(pos, std::string("\n").size(), "\n#");
		pos = s.find("\n", pos + std::string("\n#").size());
	}

	return s;
}

/*
 *		wdd
 *		Jan 6th 2021
 * */
void* Matrix::_range_convolve(void* targ){

	ConvolutionThreadSuitcase_m* localData;
	localData = (ConvolutionThreadSuitcase_m*) targ;

	// traverse our assigned area of the image and apply the kernel
	for (int i = localData->i_start; i < localData->i_end; i++){
		for (int k = localData->k_start; k < localData->k_end; k++){
			Atom sum = Atom(0.0);
			for (int x = 0; x <  localData->M.Width(); x++){
				for (int y = 0; y < localData->M.Length(); y++){
					Atom matrixValue = *(localData->M.ReadAt(x, y));		
					Atom simpleValue = *(localData->_this->ReadAt(i-x  , k - y ));
					sum.value += matrixValue.value * simpleValue.value;
				}
			}	
			// if the sum is not zero write it to the result
			if (sum.value != 0){
				Atom __value = Atom(abs(sum.value));
				localData->result_ptr->WriteAt( i, k,   __value );	
			}
		}
	}
	pthread_exit(NULL);
	return targ;  
}

/*
 *		wdd
 *		Jan 6th 2021
 * */
Matrix Matrix::_convolution(const Matrix M, const int numThreads){
	Matrix result = Matrix(this->Width(), this->Length());
	static int tid = 1;
	std::stack<ConvolutionThreadSuitcase_m*> stkThrdData = std::stack<ConvolutionThreadSuitcase_m*>();
	std::stack<pthread_t*> stkThreads = std::stack<pthread_t*>();		
	unsigned pieceWidth = this->Width()/numThreads;

	for (int i = 0; i < numThreads; i++){
		stkThrdData.push( new ConvolutionThreadSuitcase_m(
					tid++, 
					M, 
					this, 	
					((pieceWidth*i) == 0)? 2: pieceWidth*i,    
					(pieceWidth*i)+pieceWidth,    
					3, 	
					this->Length()-M.Length(), 
					&result
					)
				);
		
		stkThreads.push(new pthread_t);
		pthread_create(stkThreads.top(), NULL, Matrix::_range_convolve, stkThrdData.top()->AsVoidPointer());
	
	}
	
	while (stkThreads.empty() == false){
		pthread_t* catcher = stkThreads.top();
		stkThreads.pop();

		int ii;	
		pthread_join(*catcher, (void **)&ii); // join thread (ensures all threads are completed)
		
		if (catcher != nullptr){
			delete catcher;	
		}
	}
	while (stkThrdData.empty() == false){
		ConvolutionThreadSuitcase_m* catcher = stkThrdData.top();
		stkThrdData.pop();
		if (catcher != nullptr){
			delete catcher;
		}
	}
	return result;		

}

/*	January 7th 2021
 *	Expand()
 *	William Doyle
 *
 * */
Matrix Matrix::Expand(const unsigned inflation_value){
	Matrix xpnd = Matrix(this->Width(), this->Length()*inflation_value);
	for (unsigned i = 0; i < xpnd.Width(); i++){
		unsigned sqzd_pos = 0;
		for (unsigned k = 0; k < xpnd.Length()-1; k++){
			*(xpnd.Access(i, k)) = *(this->Access(i, sqzd_pos));
			if (k %inflation_value == inflation_value - 1)
				sqzd_pos++;
		}
	}
	return xpnd;



}
