#include "ThreadSuitcase.hpp"

/*
 *	ThreadSuitcase.cpp
 *	William Doyle
 *	December 3rd 2020
 *
 *
 *
 * */

ThreadSuitcase::ThreadSuitcase (int tid) {
	this->thread_id = tid;
};

/*	AsVoidPointer()
 *	WDD
 *
 * */
void* ThreadSuitcase::AsVoidPointer(){
	return (void*)this;
};
