#include "ConvolutionThreadSuitcase.hpp"


/*
 *	December 3rd 2020
 *	WDD
 *	Return void reference to self
 *
 * */
void* ConvolutionThreadSuitcase::AsVoidPointer() {
	return (void*)this;
};

/*	Constructor
 *	WDD
 *
 *
 * */
ConvolutionThreadSuitcase::ConvolutionThreadSuitcase(
				int tid, 
				Matrix _m, 
				GreyScaleImage* __this, 
				unsigned _is,
			       	unsigned _ie, 
				unsigned _ks, 
				unsigned _ke, 
				GreyScaleImage* _resultptr
				) : ThreadSuitcase(tid){
	this->M = _m;
	this->_this = __this;
	this->i_start = _is;
	this->i_end = _ie;
	this->k_start = _ks;
	this->k_end = _ke;
	this->result_img_ptr = _resultptr;

};


ConvolutionThreadSuitcase_m::ConvolutionThreadSuitcase_m(
				int tid, 
				Matrix _m, 
				Matrix* __this, 
				unsigned _is,
			       	unsigned _ie, 
				unsigned _ks, 
				unsigned _ke, 
				Matrix* _resultptr
				) : ThreadSuitcase(tid){
	this->M = _m;
	this->_this = __this;
	this->i_start = _is;
	this->i_end = _ie;
	this->k_start = _ks;
	this->k_end = _ke;
	this->result_ptr = _resultptr;

};

