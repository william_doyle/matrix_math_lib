#include "Pixel.hpp"

/*
 *	William Doyle
 *	January 8th 2021
 *	Pixel.cpp
 *	Implement pixel functions
 *
 * */


/*
 *	Wdd
 *	January 8th 2021
 *	3 arg pixel constructor 
 *	takes 3 Atoms
 * */
Pixel::Pixel(Atom _red, Atom _green, Atom _blue){
	this->red = std::shared_ptr<Atom>(new Atom(_red.value));
	this->green = std::shared_ptr<Atom>(new Atom(_green.value));
	this->blue = std::shared_ptr<Atom>(new Atom(_blue.value));
}

/*
 *	Wdd
 *	January 8th 2021
 *	3 arg pixel constructor 
 *	takes 3 shared pointers of Atoms
 * */
Pixel::Pixel(std::shared_ptr<Atom> _pred, std::shared_ptr<Atom> _pgreen, std::shared_ptr<Atom> _pblue){
	this->red = _pred; 
	this->green = _pgreen;
	this->blue = _pblue;
}




/*
 *	Wdd
 *	January 8th 2021
 *	pixel destructor
 * */
Pixel::~Pixel(){
}

//	G E T   C O L O R   F U N C T I O N S 
/*
 *	Wdd
 *	January 8th 2021
 *	GetRed()
 * */
Atom Pixel::GetRed(){
	return *(this->red);
}

/*
 *	Wdd
 *	January 8th 2021
 *	GetGreen()
 * */
Atom Pixel::GetGreen(){
	return *(this->green);
}

/*
 *	Wdd
 *	January 8th 2021
 *	GetBlue()
 * */
Atom Pixel::GetBlue(){
	return *(this->blue);
}

//	A C C E S S   C O L O R   F U N C T I O N S
/*
 *	Wdd
 *	January 8th 2021
 *	AccessRed()
 * */
std::shared_ptr<Atom> Pixel::AccessRed(){
	return this->red;
}

/*
 *	Wdd
 *	January 8th 2021
 *	AccessGreen()
 * */
std::shared_ptr<Atom> Pixel::AccessGreen(){
	return this->green;
}

/*
 *	Wdd
 *	January 8th 2021
 *	AccessBlue()
 * */
std::shared_ptr<Atom> Pixel::AccessBlue(){
	return this->blue;
}

//	M O N O P I X E L    F U N C T I O N S
/*
 *	Wdd
 *	January 8th 2021
 *	Monopixel constructor (single arg)
 * */
MonoPixel::MonoPixel(Atom _value): Pixel(_value, _value, _value){
	this->value = std::shared_ptr<Atom>(new Atom(_value.value));
}

/*
 *	Wdd
 *	January 8th 2021
 *	Monopixel destructor
 * */
MonoPixel::~MonoPixel(){
}

/*
 *	Wdd
 *	January 8th 2021
 *	GetValue()
 *	Gets the value of this pixel (mono pixels have only one value (no real sub pixels))
 * */
Atom MonoPixel::GetValue(){
	return *(this->value);
}

/*	Wdd
 *	January 8th 2021
 *	The following 3 functions (GetRed(), GetGreen(), GetBlue()) allow 
 *	MonoPixels to be treated like pixels that just happen to have sub pixels
 *	of the same value as eachother. These functions return by value.
 * */
Atom MonoPixel::GetRed(){
	return this->GetValue();
}
Atom MonoPixel::GetGreen(){
	return this->GetValue();
}
Atom MonoPixel::GetBlue(){
	return this->GetValue();
}


/*
 *	Wdd
 *	January 8th 2021
 *	AccessValue()
 *	Gets the pointer to this pixel's value (mono pixels have only one value (no real sub pixels))
 * */
std::shared_ptr<Atom> MonoPixel::AccessValue(){
	return this->value;
}

/*	Wdd
 *	January 8th 2021
 *	The following 3 functions (AccessRed(), AccessGreen(), AccessBlue()) allow 
 *	MonoPixels to be treated like pixels that just happen to have sub pixels
 *	of the same value as eachother. These functions return by reference.
 * */
std::shared_ptr<Atom> MonoPixel::AccessRed(){
	return this->AccessValue();
}
std::shared_ptr<Atom> MonoPixel::AccessGreen(){
	return this->AccessValue();
}
std::shared_ptr<Atom> MonoPixel::AccessBlue(){
	return this->AccessValue();
}
