#include "Matrix.hpp"
#include "MatrixMultiplication.hpp"
#include "Kernel.hpp"
#include "GreyScaleImage.hpp"
#include <ostream>
#include <iostream>
#include <fstream>
#include <sstream>
#include <assert.h>
#include <chrono>

void RegenerateLhsAndRhsTestCsvFiles();


/*	StrassenMultiply()
 *	William Doyle
 *	December 22nd 2020
 *	Try a clean take of this function. If I can get this working I'll plug it in to the operator.
 *
 *
 *
 * */
Matrix StrassenMultiply (Matrix* A, Matrix* B, Matrix* result){
	if (A->Size() == 1){
	//	result = new Matrix(1,1);
	//	*(result->Access(0, 0)) =  *(A->Access(0,0)) * *(B->Access(0,0)) ;
		return *result;
	}
	
	const unsigned sentinal = 4;
	if ((A->Width() <= sentinal)||(A->Length() <= sentinal)){
//		static int count_log = 0;
//		std::cout << "count_log " << count_log++ << " size " << A->Size() << "\n";
	//	return slow_mult(A, *B);
		return NaiveMultiply(A,B);
	}	

	//	M A K E   S U B - M A T R I C I E S 
	Matrix a = A->SubMatrixByQuarter(0);
	Matrix b = A->SubMatrixByQuarter(1);
	Matrix c = A->SubMatrixByQuarter(2);
	Matrix d = A->SubMatrixByQuarter(3);

	Matrix e = B->SubMatrixByQuarter(0);
	Matrix f = B->SubMatrixByQuarter(1);
	Matrix g = B->SubMatrixByQuarter(2);
	Matrix h = B->SubMatrixByQuarter(3);
	

//	Matrix topleft = StrassenMultiply(&a, &e, &temp) + StrassenMultiply(&b, &g, &temp);
//	Matrix topleft = NaiveMultiply(&a, &e) + NaiveMultiply(&b, &g);
	Matrix topleft =  NaiveMultiply(&b, &g) + NaiveMultiply(&a, &e);

//	Matrix topright = StrassenMultiply(&a, &f, &temp) +  StrassenMultiply(&b, &h, &temp);
//	Matrix topright = NaiveMultiply(&a, &f) +  NaiveMultiply(&b, &h);
	Matrix topright =  NaiveMultiply(&h, &b) + NaiveMultiply(&a, &f);

//	Matrix bottomleft = StrassenMultiply(&c, &e, &temp) + StrassenMultiply(&d, &g, &temp);
	Matrix bottomleft = NaiveMultiply(&c, &e) + NaiveMultiply(&d, &g);

//	Matrix bottomright = StrassenMultiply(&c, &f, &temp) + StrassenMultiply(&d, &h, &temp);
	Matrix bottomright = NaiveMultiply(&c, &f) + NaiveMultiply(&d, &h);

//	Matrix lefthalf = topleft.Glue("below", bottomleft);
//	Matrix righthalf = topright.Glue("below", bottomright);

	Matrix lefthalf = bottomleft.Glue("above", topleft);
	Matrix righthalf = bottomright.Glue("above", topright);
	
	*result = lefthalf.Glue("right", righthalf);
	return *result;

};


//#define DO_SMALL_MULT_TESTS
//#define DO_MEDIUM_MULT_TESTS

int main () {

		
#ifdef DO_SMALL_MULT_TESTS
		double arr [9] =  {  
	  	0, 1, 8,
	  	5, 2, 9,
	   	3, 7, 4 
	}; 
	Matrix mt1 = Matrix(3, 3, arr);
	std::cout << "mt1  " << mt1 << "\n";


	{
		double lhsvalues [3*2] = {1, 2, 3, 4, 5, 6};
		Matrix lhs = Matrix(2, 3, lhsvalues);
		std::cout << "lhs " << lhs << "\n";
		double rhsvalues [2*3] = {7, 8, 9, 10, 11, 12};
		Matrix rhs = Matrix(3, 2, rhsvalues);
		std::cout << "rhs " << rhs << "\n";

		Matrix result = slow_mult(&lhs, rhs);
	//	Matrix strasresult = lhs*rhs;

		double known_answer_values [2*2] = {58, 64, 139, 154};
		Matrix known_answer = Matrix(2, 2, known_answer_values);
		
	//	std::cout << "fast result " << strasresult << "\n";
		std::cout << "slow result " << result << "\n";
		std::cout << "expected result " << known_answer << "\n";
		std::cout << "difference " << known_answer - result << "\n";
	}
#endif
#ifdef DO_MEDIUM_MULT_TESTS
	{
		std::cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`medium tests`\n";	
//		RegenerateLhsAndRhsTestCsvFiles();

		Matrix lhs = Matrix::FromCsvFile("../data/20by20lhsTEST.csv");
		std::cout << "lhs " << lhs << "\n";

		Matrix rhs = Matrix::FromCsvFile("../data/20by20rhsTEST.csv");
		std::cout << "rhs " << rhs << "\n";

		Matrix result = NaiveMultiply(&lhs, &rhs);
		Matrix strasresult = lhs*rhs;

		Matrix known_answer = Matrix::FromCsvFile("../data/TEST_SOLUTION.csv");
	
		assert(known_answer == result);

		std::cout << "fast result " << strasresult << "\n";
		std::cout << "slow result " << result << "\n";
	//	std::cout << "expected result " << known_answer << "\n";
		std::cout << "difference " << strasresult - known_answer << "\n";
		std::cout << "\nend~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`medium tests`\n\n";	
	}
#endif

	int size = 16;//32;//20;//40;//160; // the pattern in difference can easily be seen with this value at 20
	int width = size;
	int length = size;

//	srand(time(NULL));

	Matrix* m1 = new Matrix (width, length); 
	Matrix* m2 = new Matrix (width, length); 
	Matrix result;
	

	m1->PopulateRandomValuesInRange(0,9);
	m2->PopulateRandomValuesInRange(0,9);

//	std::cout << "m1" << *m1 << "\n";			
//	std::cout << "m2" << *m2 << "\n";

	Matrix temp1 = NaiveMultiply(m1, m2);	
	std::cout << "slow mult" << temp1;//slow_mult(m1, *m2);
   
	Matrix temp2 = StrassenMultiply(m1, m2, &result);	
	std::cout << "new techneique" << temp2;
	
	Matrix difference = temp2 - temp1;	
	std::cout << "Difference: " << difference << "\n\n";
	GreyScaleImage(difference).Save("../images/output/more_tests_difference.ppm");
	

	delete m1;
	delete m2;
	


	return 0;
};



void RegenerateLhsAndRhsTestCsvFiles(){
	Matrix m = Matrix(20,20);
	m.PopulateRandomValuesInRange(1,9);
	m.SaveAsCsvFile("../data/20by20lhsTEST.csv");
	
	Matrix m2 = Matrix(20,20);
	m2.PopulateRandomValuesInRange(1,9);
	m2.SaveAsCsvFile("../data/20by20rhsTEST.csv");
	std::cout << "Remember to verify solution in solution file\n";
	NaiveMultiply(&m, &m2).SaveAsCsvFile("../data/TEST_SOLUTION.csv");
};
