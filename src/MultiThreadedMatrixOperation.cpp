#include "MultiThreadedMatrixOperation.hpp"
#include "DoThreadSuitcase.hpp"
#include <cmath>
/*
 *	Wdd
 *	December 5th 2020
 *	MultiThreadedMatrixOperation.cpp
 *
 * */


/*
 *
 *
 *	The magic sauce
 * */
void* MultiThreadedMatrixOperation::range_do(void* targ) {
	// unpack suitcase
	DoThreadSuitcase* localData = (DoThreadSuitcase*)targ;

	// traverse and apply
	for (unsigned i = localData->start_i; i < localData->end_i; i++){
		for (unsigned k = localData->start_k; k < localData->end_k; k++){

			localData->result->WriteAt(i, k, 
					localData->FUNCTION(*(localData->matrix1->ReadAt(i,k)), *(localData->matrix2->ReadAt(i,k)))
				);
		}
	}

	pthread_exit(NULL);

	return targ;	


};


/**
 *	ComputeOperation()
 *	wdd
 *
 * */
Matrix MultiThreadedMatrixOperation::ComputeOperation(Atom(*f)(Atom, Atom)) {
	Matrix m1 = this->operands.top();
	this->operands.pop();
	Matrix m2 = (Matrix)this->operands.top();
	this->operands.pop();

	static int tid = 1;
	Matrix RESULT = Matrix (m1.Width(), m1.Length()) ;
	
	// How many threads should we use
	this->number_threads = 4;//(m1.Width() > 8 )? 8:1; // be smarter... needs to be a factor of matrix width?
	std::stack<pthread_t*> stkThreads = std::stack<pthread_t*>();	
	std::stack<DoThreadSuitcase*> stkThrdData = std::stack<DoThreadSuitcase*>();

	// pack bag and launch threads
	unsigned piece_width = m1.Width()/sqrt(this->number_threads);
	unsigned piece_length = m1.Length()/sqrt(this->number_threads);
	
	unsigned missed_w = m1.Width() - (piece_width*sqrt(this->number_threads));
	unsigned missed_l = m1.Length() - (piece_length*sqrt(this->number_threads));

	// make up for missed elements with 2 very special additional threads	
	if ((missed_w )||(missed_l)){
		unsigned si = piece_width*sqrt(this->number_threads);
		unsigned sk = piece_length*sqrt(this->number_threads);
		
		if (missed_w){
			stkThrdData.push(new DoThreadSuitcase(tid++, &RESULT, &m1, &m2, si, m1.Width(), 0, m1.Length(), f));
			stkThreads.push(new pthread_t);
			pthread_create(stkThreads.top(), NULL, MultiThreadedMatrixOperation::range_do, stkThrdData.top()->AsVoidPointer());
			
		}
		if (missed_l){
			stkThrdData.push(new DoThreadSuitcase(tid++, &RESULT, &m1, &m2, 0, m1.Width(), sk, m1.Length(), f));
			stkThreads.push(new pthread_t);
			pthread_create(stkThreads.top(), NULL, MultiThreadedMatrixOperation::range_do, stkThrdData.top()->AsVoidPointer());
		}

	}
	for (unsigned i = 0; i < sqrt(number_threads); i++) {
		for (unsigned k = 0; k < sqrt(number_threads); k++) {
			// pack suitcase and shove it onto the pile
			stkThrdData.push(
				new DoThreadSuitcase(
						tid++,				// Thread ID
						&RESULT,			// ptr to result
						&m1,				// first matrix
						&m2,				// second matrix
						(i*piece_width),		// start i value
						(i*piece_width)+piece_width,	// end i value
						k*piece_length,			// start k value
						(k*piece_length)+piece_length,	// end k value
						f				// Function to apply to atoms

					)

				);
			// push new thread
			stkThreads.push(new pthread_t);
			// launch thread
			pthread_create(stkThreads.top(), NULL, MultiThreadedMatrixOperation::range_do, stkThrdData.top()->AsVoidPointer());
		}

	
	}

	// join pthreads and clean up memory
	while (stkThreads.empty() == false) {

		// join threads		
		pthread_t* catcher = stkThreads.top();
		stkThreads.pop();

		int ii;	
		pthread_join(*catcher, (void **)&ii); 		
		if (catcher != nullptr){
			delete catcher;	
		}

	}
	while (stkThrdData.empty() == false) {
		DoThreadSuitcase* catcher = stkThrdData.top();
		stkThrdData.pop();
		if (catcher != nullptr){
			delete catcher;
		}
	}
	return RESULT; 

};
