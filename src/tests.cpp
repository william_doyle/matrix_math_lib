/*

	November 20th 2020
	Tests the libery
	WDD
*/

#include "Matrix.hpp"
#include "MatrixMultiplication.hpp"
#include "GreyScaleImage.hpp"
#include "Kernel.hpp"

// INPUT OUTPUT RESOURSES
#include <ostream>
#include <iostream>
#include <fstream>
#include <sstream>
#include <assert.h>
#include <chrono>	// used for testing time improvments

// add ncurses to test output to show mad skills (green passed, red failed. keep it simple)

#define ATOM_RANGE 7
//#define DO_ALL_TESTS
#ifdef DO_ALL_TESTS
	#define DO_KNOWN_PASSES
	#define DO_SMALL_MULT_TESTS
	#define TEST_SUB_MATRIX	
	#define CSV_TESTS
#endif
#define CSV_TESTS
int main () {
	std::cout << "\nTests for the matrix math system.\nAuthor William Doyle \nWinter 2020\n\n";
	srand(time(NULL));
	const int size = 1001;




	


#ifdef CSV_TESTS
	{
		// Read in a CSV file as a matrix
		Matrix m1 = Matrix::FromCsvFile("../data/matrix_data_one.csv");
//		std::cout << "M1: \n" << m1 << "\n";
		m1.SaveAsCsvFile("data/temp.csv");
		Matrix m2 = Matrix::FromCsvFile("../data/temp.csv");

//		std::cout << "Here's my data:\n" <<	"M1: \n" << m1 << "\n" << "M2: \n" << m2 << "\n";
		
		if (m1 == m2){
			std::cout << "csv read write looks good.\n";
		}
		else {
			std::cout << "csv read write sanity check failed! Here's my data:\n" <<
				"M1: \n" << m1 << "\n" << "M2: \n" << m2 << "\n";
		}
	}
#endif








#ifdef DO_KNOWN_PASSES
	std::cout << "\nTest Suit One: Addition\n";
	{
		std::cout << "Test A. Small Matrix Addition: ";		
		Matrix m1 = Matrix(4, 4);
		Matrix m2 = Matrix(4, 4);
		for (unsigned i = 0; i < m1.Width(); i++){
			for (unsigned k = 0; k < m1.Length(); k++){
				m1.WriteAt(i, k, rand()%ATOM_RANGE);
				m2.WriteAt(i, k, rand()%ATOM_RANGE);
			}	
		}	
		Matrix m3 = m1+m2;
		if (m3.GetSum() == m1.GetSum() + m2.GetSum())
			std::cout << "passed.\n";
		else 
			std::cout << "failed.\n";

		std::cout << "Test B. Large Matrix Addition: ";		
		m1 = Matrix(size, size);
		m2 = Matrix(size, size);
		for (unsigned i = 0; i < m1.Width(); i++){		// populate matricies
			for (unsigned k = 0; k < m1.Length(); k++){
				m1.WriteAt(i, k, rand()%ATOM_RANGE);
				m2.WriteAt(i, k, rand()%ATOM_RANGE);
			}	
		}
		m3 = m1+m2;		// what we are actually trying to test
		if (m3.GetSum() == m1.GetSum() + m2.GetSum()) {
	//		if (m3 == m1 + m2)
				std::cout << "passed.\n";
		}
		else{ 
			std::cout << "failed.\n";
			std::cout << m3 <<"\n";

		}

		std::cout << "Test C. Commutative Property of Addition: ";
		if ((m1+m2).GetSum() == (m2+m1).GetSum())
			std::cout << "passed.\n";
		else 
			std::cout << "failed.\n";


	}
	std::cout << "\nTest Suit Two: Subtraction\n";
	{
		std::cout << "Test A. Small Matrix Subtraction: ";
		Matrix m1 = Matrix(4, 4);
		Matrix m2 = Matrix(4, 4);
		for (unsigned i = 0; i < m1.Width(); i++){
			for (unsigned k = 0; k < m1.Length(); k++){
				m1.WriteAt(i, k, rand()%ATOM_RANGE);
				m2.WriteAt(i, k, rand()%ATOM_RANGE);
			}	
		}	
		Matrix m3 = m1-m2;
		if (m3.GetSum() == m1.GetSum() - m2.GetSum())
			std::cout << "passed.\n";
		else 
			std::cout << "failed.\n";
		
		std::cout << "Test B. Large Matrix Subtraction: ";
		m1 = Matrix(size, size);
		m2 = Matrix(size, size);
		for (unsigned i = 0; i < m1.Width(); i++){		// populate matricies
			for (unsigned k = 0; k < m1.Length(); k++){
				m1.WriteAt(i, k, rand()%ATOM_RANGE);
				m2.WriteAt(i, k, rand()%ATOM_RANGE);
			}	
		}
		m3 = m1-m2;		// what we are actually trying to test
		if (m3.GetSum() == m1.GetSum() - m2.GetSum())
			std::cout << "passed.\n";
		else 
			std::cout << "failed.\n";

		std::cout << "Test C. Non Commutative Property of Subtraction: ";
		if ((m1-m2).GetSum() != (m2-m1).GetSum())
			std::cout << "passed.\n";
		else 
			std::cout << "failed.\n";
	}



#endif























	std::cout << "\nTest Suit Three: Multiplication\n";
	{




#ifdef DO_SMALL_MULT_TESTS
		std::cout << "Test A. Small Matrix Multiplication: \n source: https://www.mathsisfun.com/algebra/matrix-multiplying.html \n";
		Matrix m2 = Matrix(3, 2);
		Matrix m1 = Matrix(2, 3);

		m1.WriteAt(0,0, Atom(1));
		m1.WriteAt(0,1, Atom(2));
		m1.WriteAt(0,2, Atom(3));
		
		m1.WriteAt(1,0, Atom(4));
		m1.WriteAt(1,1, Atom(5));
		m1.WriteAt(1,2, Atom(6));

		std::cout << "m1\n" << m1 << "\n";

		m2.WriteAt(0, 0, Atom(7));
		m2.WriteAt(0, 1, Atom(8));
		m2.WriteAt(2, 0, Atom(11));
		
		m2.WriteAt(1, 0, Atom(9));
		m2.WriteAt(1, 1, Atom(10));
		m2.WriteAt(2, 1, Atom(12));
		
		std::cout << "m2\n" << m2 << "\n";
		
		Matrix m3 = m1*m2;
	
		std::cout << " m1: \n" << m1 << "\n";
		std::cout << " m2: \n" << m2 << "\n";
		std::cout << "!m3: \n" << m3 << "\n";
		std::cout << "check: \n" << m2*m1 << "\n";
		
		
		std::cout << "Test A2: Different small matrix multiplication: \n";

		{
			std::cout << "based on example found at https://www.onlinemathlearning.com/matrix-multiplication.html\n";
			Matrix mx1 = Matrix(2,3);
			Matrix mx2 = Matrix(3,2);
			
			mx1.WriteAt(0,0, Atom(1));
			mx1.WriteAt(0,1, Atom(2));
			mx1.WriteAt(0,2, Atom(-1));
			
			mx1.WriteAt(1,0, Atom(2));
			mx1.WriteAt(1,1, Atom(0));
			mx1.WriteAt(1,2, Atom(1));

			std::cout << "mx1\n" << mx1 << "\n";

			mx2.WriteAt(0, 0, Atom(3));
			mx2.WriteAt(0, 1, Atom(1));
			mx2.WriteAt(2, 0, Atom(-2));
			
			mx2.WriteAt(1, 0, Atom(0));
			mx2.WriteAt(1, 1, Atom(-1));
			mx2.WriteAt(2, 1, Atom(3));

			std::cout << "mx2\n" << mx2 << "\n";

			Matrix mx3 = mx1*mx2;
			std::cout << "mx3(Result):\n" << mx3 << "\n";
			
			Matrix mx4 = mx2*mx1;
			std::cout << "mx4(Commutability check Result):\n" << mx4 << "\n";


		}
	
		std::cout << "Test A3: Different small matrix multiplication: \n";

		{
			Matrix mx1 = Matrix(2,3);
			Matrix mx2 = Matrix(3,2);
			
			mx1.WriteAt(0,0, Atom(rand()%10));
			mx1.WriteAt(0,1, Atom(rand()%11));
			mx1.WriteAt(0,2, Atom(rand()%12));
			
			mx1.WriteAt(1,0, Atom(rand()%122));
			mx1.WriteAt(1,1, Atom(rand()%2));
			mx1.WriteAt(1,2, Atom(rand()%30));

			std::cout << "mx1\n" << mx1 << "\n";

			mx2.WriteAt(0, 0, Atom(rand()%3));
			mx2.WriteAt(0, 1, Atom(rand()%2));
			mx2.WriteAt(2, 0, Atom(rand()%4));
			
			mx2.WriteAt(1, 0, Atom(rand()%100));
			mx2.WriteAt(1, 1, Atom(rand()%6));
			mx2.WriteAt(2, 1, Atom(rand()%33));

			std::cout << "mx2\n" << mx2 << "\n";

			Matrix mx3 = mx1*mx2;
			std::cout << "mx3(Result):\n" << mx3 << "\n";


		}

		std::cout << "Test B. Random Matrix Multiplication: \n\n";	
		m1 = Matrix(10, 5);
		m2 = Matrix(5, 10);
		for (unsigned i = 0; i < m1.Width(); i++){		// populate matricies
			for (unsigned k = 0; k < m1.Length(); k++){
				m1.WriteAt(i, k, rand()%ATOM_RANGE);
				m2.WriteAt(k, i, rand()%ATOM_RANGE);
			}	
		}
		
		std::cout << "\tmatrix 1: \n" <<  m1 << "\n";
		std::cout << "\tmatrix 2: \n" <<  m2 << "\n";
//		m3 = m2*m1;		
		m3 = m1*m2;		
		std::cout << "\tmatrix 3 ('matrix 1' x 'matrix 2'): \n" <<  m3 << "\n";
/*		for (int i = 0; i < m1.Width(); i++)
			for (int k = 0; k < m1.Length(); k++)
				// check all of the work. if anything is wrong say "failed" and goto fail point 2
				if (( (m1.ReadAt(i,k)->value)*(m2.ReadAt(i,k)->value) ) == m3.ReadAt(i,k)->value)
					continue;
				else{ 
					std::cout << "failed. (" << i << ", " << k << ")\n";
					goto FAILURE_REENTRY_POINT2; // is this worth it? Consider when cleaning
				}
		std::cout << "passed.\n";
	FAILURE_REENTRY_POINT2:

	*/
		std::cout << "Test C. Commutative Property of Multiplication: ";
		if ((m1*m2).GetSum() == (m2*m1).GetSum())
			std::cout << "passed.\n";
		else 
			std::cout << "failed. difference ('error') indicator:" << (m1*m2).GetSum() - (m2*m1).GetSum() << "\n";

		std::cout << "\n";


#endif














		// sorry



















		std::cout << "Test D: Fast Multiplication\n";
		{


			int long_side_length =  20;	// good test @ 22
			int short_side_length = 20;	// good test at 17
		





//			std::cout << "Input area is " << long_side_length * short_side_length << "\n";

			Matrix bigA = Matrix(long_side_length, short_side_length);
			Matrix bigB = Matrix(short_side_length, long_side_length);


			for (unsigned i = 0; i < bigA.Width(); i++){		// populate matricies
				for (unsigned k = 0; k < bigA.Length(); k++){
					bigA.WriteAt(i, k, rand()%ATOM_RANGE);
					bigB.WriteAt(k, i, rand()%ATOM_RANGE);
				}	
			}
		
			std::cout << "Big A:\n" << bigA << "\n";			// show the matrices to the user	
			std::cout << "Big B:\n" << bigB << "\n";	
			
			Matrix bigC = bigA*bigB;
			


#ifdef TEST_SUB_MATRIX
			//	ALLOW USER TO EXAMIN THE RESULTS OF THE FOUR CALLS TO THE SUB MATRIX BY QUARTER FUNCTION 
			std::cout << "top left \n" << bigC.SubMatrixByQuarter(0); 	
			std::cout << "top right \n" << bigC.SubMatrixByQuarter(1); 	
			std::cout << "bottom left \n" << bigC.SubMatrixByQuarter(2); 	
			std::cout << "bottom right" << bigC.SubMatrixByQuarter(3);
#endif



			std::cout << "Big C (solution):\n" << bigC << "\n";
//			GreyScaleImage(bigC).Save("images/tests/BigC.ppm");	
/*			if ((bigA*bigB) == (bigB*bigA))
				std::cout << "commutative property test passed\n";
			else
				std::cout << "commutative property test failed\n";
*/
			Matrix comp = slow_mult(&bigA, bigB);
	//		GreyScaleImage(comp).Save("images/tests/Compair.ppm");	
			std::cout << "Slow Mult comparison (truth):\n" << comp << "\n";
			
//			MatrixMultiplication::pointer_multiply(&bigA, &bigB, &comp, 0, bigB.Width(), 0, bigA.Length(), 0, bigA.Width());
//			std::cout << "ptr func test:\n" << comp << "\n";
			
			Matrix mDif = comp - bigC;
//			std::cout << "difference: " << mDif << "\nsum: " << mDif.GetSum() << "\navg err: "<< mDif.GetAverage() <<"\n";
		


			//	R E C O R D   D A T A 
			// save average error to file
			std::ofstream outfile;
			outfile.open("data/averageError.txt", std::ios_base::app);	// open file in append mode
			outfile << ATOM_RANGE << ", " << long_side_length << ", " << short_side_length << ", " << mDif.GetAverage() << "\n";
			outfile.close();											// close file




			
	//		GreyScaleImage(mDif).Save("images/tests/dif.ppm");	
	//		comp = slow_mult(&bigB, bigA); // looks like slow multiplication is broken 
	//		std::cout << "(reality check) Slow Mult comparison:\n" << comp << "\n";
			





























		}




	}

	


};
