#include "MatrixMultiplication.hpp"
#include <assert.h>
#include <iostream>
#include <fstream>

#include <stdio.h>  // for deleting file:  https://www.cplusplus.com/reference/cstdio/remove/
					// https://stackoverflow.com/questions/17032970/clear-data-inside-text-file-in-c
					// Answer by Thomas Matthews on Jun 10'13@21:35 makes more sense. Thus I'm going to 
					// use it.
					//
					// used for remove("filename.txt") function
/*
 * December 11th 2020
 * 
 * Wdd
*/

/*
 *  two arg constructor
 *	December 12th 2020
 *	wdd
 *
 * */
MatrixMultiplication::MatrixMultiplication(Matrix _a, Matrix _b){
	this->operands.push(_b);	// the final version MUST FORGO ALL THESE POINTLESS DATA STRUCTURES!
	this->operands.push(_a);
};

/*
 * December 12th 2020
 * wdd
 * zero arg constructor
 * */
MatrixMultiplication::MatrixMultiplication(){ }

/*	wdd
 *	December 12th 2020
 *	simple destructor
 *
 * */
MatrixMultiplication::~MatrixMultiplication(){ }

/**
 *	Find The Max of three unsigned integers
 *	William Doyle
 *	December 11th or 12th 2020
 *
 * */
unsigned MatrixMultiplication::Max(unsigned n, unsigned m, unsigned p){
	if ( (n > m) && (n > p) )
		return n;
	else if ( (m > n) && (m > p) )
		return m;
	else if ( (p > n) && (p > m) )
		return p;
	return p;
};


/*	December 13th 2020
 * 	William Doyle
 *	static
 *
 * */
void MatrixMultiplication::pointer_multiply(Matrix* a, Matrix* b, Matrix* result, 
			unsigned ISTART,
			unsigned IEND,
			unsigned JSTART,
			unsigned JEND,
			unsigned KSTART,
			unsigned KEND
		){
	for (unsigned i = ISTART; i < IEND; i++){
		for (unsigned j = JSTART; j < JEND; j++){
			double sum = 0.0;
			for (unsigned k = KSTART; k < KEND; k++){
				sum += a->ReadAt(k,i)->value * b->ReadAt(j,k)->value;
			}
			
			static bool called = false;
			if (called == false){
				if (remove("data/visited.txt") != 0){
					std::cerr << "Problem deleting file \"data/visited.txt\"\n";
				}	
				called = true;
			}

//			std::ofstream outfile;
//			outfile.open("data/visited.txt", std::ios_base::app);	// open file in append mode
//			outfile << j << ",\t" << i << ",\t@" << result << "\n";
//			outfile.close();

			if ( result->WriteAt(j,i, Atom(sum)) == false ){
				static int errcont = 0;
				errcont++;
				std::cout << "Error in \'" << __func__ << "\'. Failed to Write to RESULT at " << j << ", " << i << "\nec:" << errcont << "\n"; 
			}
		}

	}
	return;	

};

/*	December 13th 2020
 *	wdd
 *	recursive_multiply()
 *
 *	static
 * */
void MatrixMultiplication::recursive_multiply(Matrix* a, Matrix* b, Matrix* result,
			unsigned ISTART,
			unsigned IEND,
			unsigned JSTART,
			unsigned JEND,
			unsigned KSTART,
			unsigned KEND
		){
	if (KSTART == KEND){
		return;
	}	
	std::cerr << __func__ << ": **notice**	ISTART	IEND	JSTART	JEND	KSTART	KEND\n";
	std::cerr << ISTART << ", " << IEND << ", " << JSTART << "," << JEND << ", " << KSTART << ", " << KEND << "\n"; 
	const int minimum = 7;
	if ( (IEND-ISTART < minimum) || (JEND-JSTART < minimum) || (KEND-KSTART < minimum) ) {
		/*
		 	Our piece is small enough that we shall not divide it any further
			Simply call the pointer multiply function to multiply the area of the marix 
			we are currently responsable for
		 */
		std::cout << "\tbottomed out "<< ISTART << " " << IEND << " " <<  JSTART <<" "  << JEND << " " << KSTART << " " << KEND << "\n";
		pointer_multiply(a, b, result, ISTART, IEND, JSTART, JEND, KSTART, KEND);
		std::cout << "\tAbout to return from " << __func__ << "\n";
		return;
	};
	
	std::cout << "\t\t\tRecursive Call About To Happen! note: kend " << (unsigned)(KEND/2) << ", k start " << KSTART << "\n";

	// split matrix in two
	// remember a width > b width
	
	if (KSTART < KEND/2){
		MatrixMultiplication::recursive_multiply(a, b, result, ISTART, IEND, JSTART, JEND, KSTART, (unsigned)(KEND/2));
		std::cout << "\t\tRecursive Call 1 just happend! Here goes number 2 [kstart is " <<  (unsigned)(KEND/2) << "]\n";
		MatrixMultiplication::recursive_multiply(a, b, result, ISTART, IEND, JSTART, JEND, (unsigned)(KEND/2), KEND);
		std::cout << "\t\tRecursive Call 2 over \n";
	}

		
	/*	
	  	~~~~	G	U	I	D	E	~~~~~
		unsigned i;	//		iterates through B WIDTH
		unsigned j; //		iterates through A LENGTH
		unsigned k; //		iterates through A WIDTH
	*/

	return;
};

Matrix MatrixMultiplication::ComputeOperation(Atom(*f)(Atom,Atom)) {
	// 	
	Matrix a = this->operands.top();
	this->operands.pop();
	Matrix b = this->operands.top();
	this->operands.pop();

	if (a.Width() <= b.Width()){
		Matrix temp = a;
		a = b;
		b = temp;
	}

	assert(a.Width() == b.Length());
	assert(b.Width() == a.Length());
		
	Matrix result = Matrix( (  a.Width() < b.Width()  )? a.Width():b.Width(),  (  a.Length() < b.Length()  )? a.Length():b.Length()  );

	// if matrix is small
	if (a.Size() <= 16){	// probably want to set to 100
	//if ( (   ) || ( a.Size() <= 16 )  ) {	// probably want to set to 100
		// slow multiply
		result = slow_mult(&a,b);	
		return result;
	}

//	Matrix* result_q1 = 
	
	Matrix _a, _b, _c, _d, _e, _f, _g, _h;
	_a = a.SubMatrixByQuarter(0);
	_b = a.SubMatrixByQuarter(1);
	_c = a.SubMatrixByQuarter(2);
	_d = a.SubMatrixByQuarter(3);

	_e = b.SubMatrixByQuarter(0);
	_f = b.SubMatrixByQuarter(1);
	_g = b.SubMatrixByQuarter(2);
	_h = b.SubMatrixByQuarter(3);
	

	
	Matrix resultq1, resultq2, resultq3, resultq4;


	assert(_a.Size() == _e.Size()); // add more of these to make errors obvious
	assert(_b.Size() == _g.Size());

	assert(_c.Size() == _f.Size());
	assert(_d.Size() == _h.Size());

	if ((_a.Size() != _f.Size())||(_b.Size() != _h.Size())||(_c.Size() != _e.Size())||(_d.Size() != _g.Size())){
	//	return MatrixMultiplication::pointer_multiply(a, b, result, ISTART, IEND, JSTART, JEND, KSTART, KEND);
		return slow_mult(&a, b);
	}
	assert(_a.Size() == _f.Size());
	assert(_b.Size() == _h.Size());
	assert(_c.Size() == _e.Size());
	assert(_d.Size() == _g.Size());
	
	resultq1 = (_a*_e)+(_b*_g);
	resultq2 = (_a*_f)+(_b*_h);
	resultq3 = (_c*_e)+(_d*_g);
	resultq4 = (_c*_f)+(_d*_h);

/*	std::cout << "------------------------------------------\n";
	
	std::cout << "Q1:\n" << resultq1 << "\n";
	std::cout << "Q2:\n" << resultq2 << "\n";
	std::cout << "Q3:\n" << resultq3 << "\n";
	std::cout << "Q4:\n" << resultq4 << "\n";
	
	
	std::cout << "------------------------------------------\n";
*/	
	Matrix resulth1, resulth2;
	resulth1 = resultq1.Glue("below", resultq3);
	resulth2 = resultq2.Glue("below", resultq4);
	result = resulth1.Glue("right", resulth2);

//	std::cout << "left side via glue \n" << resultq1.Glue("below", resultq3) << "\n";


//	std::cout << "test r1 \n" << resultq1 << "\n";
/*	recursive_multiply(&a, &b, &result, 
			0,			// i start 
			b.Width(),  // i end
			0,			// j start
			a.Length(),	// j end
			0,			// k start
			a.Width()	// k end
			);

			*/
	return result;



};


