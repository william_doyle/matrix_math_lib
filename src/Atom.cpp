#include "Atom.hpp"

/*
 *	December 4th 2020
 *	Wdd
 *	Atom.cpp
 *	implement functions outlined in Atom.hpp
 *
 *
 *
 * */

/*
 *	From Operation class
 *	GetType()
 *	WDD
 *	return a string detailing what this object is
 * */
std::string Atom::GetType() {
	return "Atomic Double";
};

/*
 *	Atom constructor
 *	One argument. Type is double.
 *
 * */
Atom::Atom(double __value_param){
	this->value = __value_param;
};

/*
 *	wdd
 *	default constructor of the atom class. default value is '0'
 * */
Atom::Atom() {
	this->value = 0.0;

}

/*	wdd
 *	'+' operator overload
 *	December 27th 2020
 *
 * */
Atom Atom::operator+(Atom a) {
	return Atom (  this->value + a.value  );
}

/*	wdd
 *	'-' operator overload
 *	December 27th 2020
 *
 * */
Atom Atom::operator-(Atom a) {
	return Atom (  this->value - a.value  );
}


/*	William Doyle
 *	+= operator overload of an Atom
 *	December 26th 2020
 *
 * */
Atom& Atom::operator+=(const Atom& rhs){
	this->value += rhs.value;
	return *this;
}

/*	William Doyle
 *	-= operator overload of an Atom
 *	December 26th 2020
 *
 * */
Atom& Atom::operator-=(const Atom& rhs){
	this->value -= rhs.value;
	return *this;
}

/*	wdd
 *	December 27th 2020
 *	multiplication operator overload
 * */
Atom Atom::operator*(Atom rhs){
	return Atom(  this->value * rhs.value  );	
}

/*	wdd
 *	December 27th 2020
 *	division operator overload
 * */
Atom Atom::operator/(Atom rhs){
	return Atom(  this->value / rhs.value  );
}

/*	wdd
 *	December 27th 2020
 *	division operator overload
 *
 *	allow an atom to be divided by a double
 * */
Atom Atom::operator/(double rhs){
	return (*this)/Atom(rhs);
}

/*	wdd
 *	December 27th 2020
 *	equivilency operator overload
 *
 * */
bool Atom::operator == (Atom a){
	return this->value == a.value;
}

/*	wdd
 *	December 27th 2020
 *	greater than operator overload
 *
 * */
bool Atom::operator > (Atom a){
	return this->value > a.value;
}

/*	wdd
 *	December 27th 2020
 *	lesser than operator overload
 *
 * */
bool Atom::operator < (Atom a){
	return this->value < a.value;
}

/*
 *	wdd
 *	construct an atom from an int
 * */
Atom::Atom(int _val){
	this->value = (double)_val;
};

/*
 *	wdd
 *	construct an atom from an unsigned int
 * */
Atom::Atom(unsigned ui){
	this->value = (double)ui;
}

Atom Atom::ADD_FUNC(Atom a1, Atom a2){
	return Atom(a1.value + a2.value);

}

Atom Atom::SUB_FUNC(Atom a1, Atom a2){
	return Atom(a1.value - a2.value);

}

Atom Atom::MULT_FUNC(Atom a1, Atom a2){
	return Atom(a1.value * a2.value);
}
