#include "GreyScaleImage.hpp"
#include <fstream>
#include <iostream>
#include <string>
#include <cmath>
#include <sstream>
#include <pthread.h>
#include <stack>
#include "Kernel.hpp"
#include "ConvolutionThreadSuitcase.hpp"

/*
 *	William Doyle
 *	Constructor
 *
 * */
GreyScaleImage::GreyScaleImage(unsigned _width, unsigned _length) : Matrix(_width, _length) {
	this->number_cols = _width;
	this->number_rows = _length;
//	this->values = new std::vector<Atom>();
	this->values = std::shared_ptr<std::vector<Atom>>(new std::vector<Atom>());
	
	for ( int k = 0; k < this->Width(); k++)
		for (int i = 0; i < this->Length(); i++)
			this->WriteAt(k, i, Atom(0));
}


/*
 *	william doyle
 *	GreyScaleImage from a matrix and a string 
 *
 * */
GreyScaleImage::GreyScaleImage(Matrix values,  std::string fmat) {
	
	*this = GreyScaleImage(values.Width(), values.Length());
	
	for (int i = 0; i < values.Width(); i+=1){
		for (int k = 0; k < values.Length(); k+=3){
			this->WriteAt(i, k,   abs(values.ReadAt(i,k)->value));
			if (k != values.Length()-1){
				this->WriteAt(i, k+1, abs(values.ReadAt(i,k)->value));
				this->WriteAt(i, k+2, abs(values.ReadAt(i,k)->value));
			}
		}
	}
	this->max_color_value = (unsigned)(values.Max().value);
	this->format = fmat;
	
	this->number_rows = this->Width();
	this->number_cols = this->Length()/3;
};


GreyScaleImage::~GreyScaleImage() {

};

// construct a grey scale image from a file given the files name
GreyScaleImage::GreyScaleImage(std::string filename) : Matrix() {
//	this->values = new std::vector<Atom>();
	this->values = std::shared_ptr<std::vector<Atom>>(new std::vector<Atom>());

	std::ifstream file(filename);
	std::string str;

	// get values from file into system
	unsigned long long count = 0;
	unsigned row_pos = 0;
	unsigned col_pos = 0;
	while(std::getline(file, str)){
		if (str.at(0) == '#'){
			continue;
		}
		if (count == 0){
			this->format = str;
			count++;
			continue;
		}
		if (count == 1){
			// This is the only line in the image file with two numbers on it
			// we need to split the line into two strings. The two strings are seperated by a ' ' (space) char
			std::string sCols = str.substr(0, str.find(" ") );
			std::string sRows = str.substr(str.find(" ") , str.size() - str.find(" "));
			
			// extract and save the two numbers found on this line 
			this->number_cols = std::stoi(sCols);
			this->number_rows = std::stoi(sRows);
			
			// advance counter value and continue with file
			count++;
			continue;
		}
		if (count == 2){
			// skip over the 255. For now
			this->max_color_value = std::stoi(str);
			count++;
			continue;
		}
		// this line has one number and that number is how much grey the pixel is
		values->push_back(std::stoi(str));
		count++;
	}
	
	this->Establish(this->number_rows, this->number_cols*3);// used to establish the internal model of the matrix

	for (unsigned i = 0; i < this->Width(); i+=1){
		for (unsigned k = 0; k < this->Length(); k += 1){
			if ( this->WriteAt(i, k, values->at(  (i * this->Length()) + k)) == false ){
				std::cerr << "Write failed! (i,k) (" << i << ", " << k << ")\n";
			}	
		}
	}
//	file.close();


};

/*
 *	wdd
 *	january 6th 2021
 *	___save()
 *	takes ptr to ofstream (should be open and have image format already)
 *	and the name of the file we wish to save
 *
 * */
void GreyScaleImage::___save(std::ofstream* ofs, std::string filename){

		*ofs << this->number_cols << ' '<< this->number_rows << std::endl << this->max_color_value << std::endl;	
		for (unsigned i = 0; i < this->Width(); i++) {
			for (unsigned k = 0; k < this->Length(); k++) {
				if (ofs->is_open()){
					if (this->ReadAt(i, k) == nullptr)
						std::cerr << "nullptr read from at " << i << ", " << k << "\n" ;

					*ofs << *(this->ReadAt(i, k)) << "\n";
				}
				else 
					std::cerr << "Error: The File Became Closed!\n";
			}
		}

};

/***
 *	November 2020 
 *	Wdd
 *	Save the image as a file with name filename
 * */
void GreyScaleImage::Save(std::string filename){

		std::ofstream ofs;
		ofs.open (filename, std::ios_base::out ); 
		ofs << this->format << "\n";
		this->___save(&ofs, filename);

		ofs.close();
};

/**
 *	January 5th 2021
 *	wdd
 *	Save this image and put the second string into the image file as a comment
 * */
void GreyScaleImage::Save(std::string filename, std::string infile_comments){

		std::ofstream ofs;
		ofs.open (filename, std::ios_base::out );
	   	
		ofs << this->format << "\n";					// must be done before passing to ___save()

		ofs << "#" << infile_comments << "\n";	
		this->___save(&ofs, filename);
		ofs.close();

}

/*
 * Wdd
 * November 2020
 * Set all pixel values to zero
 * */
void GreyScaleImage::Clear(){
	for ( int k = 0; k < this->Width(); k++)
		for (int i = 0; i < this->Length(); i++)
			this->WriteAt(k, i, Atom(0));
}

/**
 *	November 28th 2020
 *	Wdd
 *	ClearClone()
 *	copy this image by value and clear the pixel values of the copy
 *
 * */
GreyScaleImage GreyScaleImage::ClearClone(){
	GreyScaleImage result_img = *this;		
	result_img.Clear();
	return result_img;
}

/**
 *	Wdd
 *	Convolution()
 *	Apply a convolution on THIS image using the provided Matrix as the kernel
 *	arguments: Matrix M and int number of threads
 * */
GreyScaleImage GreyScaleImage::Convolution(const Matrix M, int numThreads){
	
	// 1. Compress down by removing redundent GB values from RGB (we have 3 subpixels but all are same value... thats how we are representing GreyScaleImages)
	Matrix sqzd = Matrix(this->number_rows, this->number_cols);
	for (unsigned i = 0; i < sqzd.Width(); i++)
		for (unsigned k = 0; k < sqzd.Length()-1; k++)
			*(sqzd.Access(i,k)) = *(this->Access(i, k*3));
	
	// 2. preform a convolution on the resulting matrix
	sqzd = sqzd._convolution(M, numThreads);
	
	// 3. re expand matrix
	// 4. use resulting matrix to create a new GreyScaleImage 
	// 5. return this GreyScaleImage
	return GreyScaleImage(sqzd.Expand(), this->format);

}

/*
 *	WDD
 *	range_convolve
 *	to be called by the function 'Convolution'. This function is designed to be ran in a seperate pthread
 *	~Early December 2020
 *
 * */
void* GreyScaleImage::range_convolve(void* targ){
	// unpack the suitcase
	ConvolutionThreadSuitcase* localData;
	localData = (ConvolutionThreadSuitcase*) targ;

	// traverse our assigned area of the image and apply the kernel
	for (int i = localData->i_start; i < localData->i_end; i++){
		for (int k = localData->k_start; k < localData->k_end; k+=3){
			Atom sum = Atom(0.0);
			for (int x = 0; x <  localData->M.Width(); x++){
				for (int y = 0; y < localData->M.Length(); y++){
					Atom matrixValue = *(localData->M.ReadAt(x, y));		
					Atom simpleValue = *(localData->_this->ReadAt(i-x  , k - y ));
					sum.value += matrixValue.value * simpleValue.value;
				}
			}	
			// if the sum is not zero write it to the result
			if (sum.value != 0){
				Atom __value = Atom(abs(sum.value));
				localData->result_img_ptr->WriteAt( i, k,   __value );	
				localData->result_img_ptr->WriteAt( i, k+1, __value );	
				localData->result_img_ptr->WriteAt( i, k+2, __value ); 	
			}
		}
	}
	pthread_exit(NULL);
	return targ;  
};


/*
 *	William Doyle
 *	Sharpen()
 *	provides a way for the user to sharpen an image without any understanding of 
 *	convolutions or kernels.
 * */
GreyScaleImage GreyScaleImage::Sharpen(){
	GreyScaleImage img = this->Convolution(Kernel::Sharpen());
	return img;
};

// operators
/*
 *	William Doyle
 *	Subtraction of another GreyScaleImage operator overload
 *
 * */
GreyScaleImage GreyScaleImage::operator - (GreyScaleImage gsi) {
	GreyScaleImage retrnval = this->ClearClone();
	unsigned width = (this->Width() < gsi.Width())? this->Width(): gsi.Width();		// get the smaller value
	unsigned length =  (this->Length() < gsi.Length())? this->Length(): gsi.Length();

	for (unsigned i = 0; i < width; i++){
		for (unsigned k = 0; k < length; k++){
			Atom pixval;
		       	pixval.value	= this->ReadAt(i,k)->value - gsi.ReadAt(i, k)->value;
			retrnval.WriteAt(i, k, pixval);

		}
	}
	return retrnval;
};

GreyScaleImage GreyScaleImage::operator + (GreyScaleImage gsi) {
	GreyScaleImage retrnval = this->ClearClone();
	unsigned width = (this->Width() < gsi.Width())? this->Width(): gsi.Width();		// get the smaller value
	unsigned length =  (this->Length() < gsi.Length())? this->Length(): gsi.Length();

	for (unsigned i = 0; i < width; i++){
		for (unsigned k = 0; k < length; k++){
			Atom pixval;
			pixval.value = this->ReadAt(i,k)->value + gsi.ReadAt(i, k)->value;

			// make pixval an absolute value and make sure it does not exceed 255
			retrnval.WriteAt(i, k, Atom( abs(pixval.value)%this->max_color_value) );

		}
	}
	return retrnval;
};


/*
 *	January 7th 2021
 *	William Doyle
 *	Create A GreyScaleImage from a color image
 *
 * */
GreyScaleImage::GreyScaleImage(const ColorImage ci){

	Matrix m (ci.number_rows, ci.number_cols);
	for (unsigned i = 0; i < m.Width(); i++){
		for (unsigned k = 0; k < m.Length(); k++){
			*(m.Access(i,k)) = static_cast<int>(((*(ci.reds->Access(i,k)) + *(ci.greens->Access(i,k)) + *(ci.blues->Access(i,k)))/3).value); 
		}
	}	

	*this = GreyScaleImage(m.Expand(3), ci.format);

}


